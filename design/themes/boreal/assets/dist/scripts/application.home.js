/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var urlRoot, videoContainer;

	urlRoot = './design/themes/boreal/assets/dist/';

	$('[header-hero]').attr('src', urlRoot + 'images/content/home-hero' + ($(window).width() < 768 ? "-mobile" : "") + '.png');

	$('.mosaic-col').matchHeight();

	$('body').on('instagram:feeded', function() {
	  $('.mosaic-col').matchHeight();
	  return setTimeout(function() {
	    return $('.mosaic-col').matchHeight();
	  }, 2000);
	});

	videoContainer = $('[vide-home]');

	if ($('html').hasClass('Explorer')) {
	  videoContainer.css('background-image', 'url(./design/themes/boreal/assets/dist/images/content/blocks/block-home-3.jpg)');
	} else {
	  videoContainer.vide({
	    mp4: './design/themes/boreal/assets/dist/videos/latte',
	    webm: './design/themes/boreal/assets/dist/videos/latte',
	    ogv: './design/themes/boreal/assets/dist/videos/latte',
	    poster: './design/themes/boreal/assets/dist/videos/latte'
	  }, {
	    posterType: "jpg",
	    loop: true,
	    muted: false,
	    position: "0% 0%"
	  });
	}


/***/ }
/******/ ]);