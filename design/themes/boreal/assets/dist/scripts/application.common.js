/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {__webpack_require__(1).bootstrap(global._App_);
	$(function() {
	  var feed, generalOnresize, loader, matchHeight, safari3ColFix, safari3ColFixDebounced, sizeup, sizeupGroup;
	  setTimeout(function() {
	    return $('.mainmenu-container').fadeIn();
	  }, 300);
	  if ((location.href.indexOf('shop') !== -1) || (location.href.indexOf('cart') !== -1) || (location.href.indexOf('checkout') !== -1)) {
	    $('body').addClass('l-shop');
	  }
	  if (location.href.indexOf('sent=Y') !== -1) {
	    App.utils.scrollTo($('[form-success] .block-side-inner'), 0);
	  }
	  (safari3ColFix = function() {
	    var currWidth, missingPixels;
	    if ($('html').hasClass('Safari')) {
	      $('.gallery').css('width', 'auto');
	      if ($(window).width() > 767) {
	        currWidth = $('.gallery').outerWidth();
	        missingPixels = 3 - (currWidth % 3);
	        if (missingPixels > 0) {
	          $('.gallery').css('width', currWidth + missingPixels);
	          return $('.gallery-item').css('width', (currWidth + missingPixels) / 3);
	        }
	      }
	    }
	  })();
	  safari3ColFixDebounced = $.debounce(300, safari3ColFix);
	  $(window).on('resize', safari3ColFixDebounced);
	  $('.header-bottom-scroll').on('click', function() {
	    return App.utils.scrollTo($('.block').first(), -98);
	  });
	  $('[newsletter-submit]').on('click', function() {
	    if (/\S+@\S+/.test($('[newsletter-email]').val())) {
	      return $('[newsletter-hidden-submit-button]').click();
	    } else {
	      return $('.newsletter-error').fadeIn();
	    }
	  });
	  $('[tab-dropdown]').on('change', function() {
	    var $el;
	    $el = $(this);
	    return $('[tab-selector]').filter('[href=' + $el.val() + ']').trigger('click');
	  });
	  $('[tab-selector]').on('click', function() {
	    var selectedContentContainer;
	    $('[tab-content]').hide().removeClass('is-active');
	    selectedContentContainer = $('[tab-content=' + $(this).attr('href').split('#')[1] + ']');
	    selectedContentContainer.show();
	    setTimeout(function() {
	      return selectedContentContainer.addClass('is-active');
	    }, 300);
	    $('#jebrew_type').val($(this).attr('href').split('#')[1]);
	    App.utils.scrollTo('.block-side--jebrew', -98);
	    $('[tab-selector]').removeClass('is-active');
	    return $(this).addClass('is-active');
	  });
	  sizeup = function() {
	    return $('[sizeup]').each(function() {
	      var $el, ratio;
	      $el = $(this);
	      ratio = $el.attr('sizeup').split(':');
	      if ((ratio[0] === 'skip-xs') && $(window).width() < 992) {
	        return;
	      }
	      if (ratio.length === 1) {
	        ratio = [1, 1];
	      }
	      return $el.css('height', Math.round(~~($el.outerWidth() * (ratio[1] / ratio[0]))));
	    });
	  };
	  sizeupGroup = function() {
	    var groups;
	    groups = [];
	    return $('[sizeup-group]').each(function() {
	      var $el, currentGroup, newHeight;
	      $el = $(this);
	      currentGroup = $el.attr('sizeup-group');
	      if (groups[currentGroup]) {
	        return $el.css('height', groups[currentGroup]);
	      } else {
	        newHeight = ~~($el.outerWidth());
	        $el.css('height', newHeight);
	        return groups[currentGroup] = newHeight;
	      }
	    });
	  };
	  matchHeight = function() {
	    $('.block-side').matchHeight();
	    return $('.mh-footerCols').matchHeight();
	  };
	  generalOnresize = function() {
	    matchHeight();
	    sizeup();
	    return sizeupGroup();
	  };
	  $(window).resize(generalOnresize);
	  generalOnresize();
	  setTimeout(function() {
	    $('body').removeClass('is-loading');
	    return generalOnresize();
	  }, 250);
	  loader = function() {
	    $('body').find('.is-init').first().removeClass('is-init');
	    if ($('body').find('.is-init').length) {
	      return setTimeout(loader, Math.random() * 200);
	    }
	  };
	  loader();
	  // if ($('[mosaic]').length) {
	  //   feed = '/social/instagram.php';
	  //   return $.get(feed, function(resp) {
	  //     $('[mosaic-item]').each(function(i, e) {
	  //       var $el;
	  //       $el = $(this);
	  //       $el.css('background-image', 'url(' + resp[i].images.standard_resolution.url + ')');
	  //       return $el.attr('href', resp[i].link);
	  //     });
	  //     return $('body').trigger('instagram:feeded');
	  //   });
	  // }
	});

	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {module.exports = global["App"] = __webpack_require__(2);
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	var App;

	App = (function() {
	  function App() {
	    this.env = 'development';
	    this.lang = 'en';
	    this.version = {
	      localStorage: .1
	    };
	  }

	  App.prototype.bootstrap = function() {
	    $.extend(this, arguments[0] || {});
	    __webpack_require__(3);
	    __webpack_require__(5).attachTo(this);
	    __webpack_require__(6).init();
	    __webpack_require__(7).init();
	    __webpack_require__(8).init();
	    __webpack_require__(9).init();
	    __webpack_require__(10).init('[data-imagelightbox]');
	    __webpack_require__(11).init();
	    __webpack_require__(12).init();
	    __webpack_require__(13).init();
	    __webpack_require__(14).attachListener();
	    __webpack_require__(15).init().addClassesOn('html').attachTo(this);
	    if (this.lang === 'fr') {
	      return __webpack_require__(16);
	    }
	  };

	  return App;

	})();

	module.exports = new App();


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(global) {module.exports = global["log"] = __webpack_require__(4);
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }())))

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = function() {};

	if (typeof console.log === 'object' && Function.prototype.bind && console) {
	  ['log', 'info', 'warn', 'error', 'assert', 'dir', 'clear', 'profile', 'profileEnd'].forEach((function(method) {
	    console[method] = this.call(console[method], console);
	  }), Function.prototype.bind);
	}


/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var Utils;

	Utils = (function() {
	  var instance;

	  instance = null;

	  Utils.prototype.hasHistoryAPI = function() {
	    return typeof history.pushState !== "undefined";
	  };

	  function Utils() {
	    if (instance) {
	      return instance;
	    } else {
	      instance = this;
	    }
	  }

	  Utils.prototype.attachTo = function() {
	    return arguments[0].utils = this;
	  };

	  Utils.prototype.scrollTo = function() {
	    var callback, offset, target;
	    target = $(arguments[0]);
	    offset = arguments[1] || 0;
	    callback = function() {};
	    if (target.length) {
	      return $('body,html').animate({
	        scrollTop: target.offset().top + offset
	      }, 600).promise().done(callback);
	    }
	  };

	  return Utils;

	})();

	module.exports = new Utils();


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	module.exports.init = function() {
	  return $('[background], [icon-url]').each(function() {
	    var url;
	    url = $(this).attr('background') || $(this).attr('icon-url');
	    if (!url) {
	      url = $(this).find('img').first().attr('src');
	      $(this).find('img').remove();
	    }
	    return $(this).css('background-image', 'url(' + url + ')');
	  });
	};


/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(7);

	module.exports.init = function() {
	  return $('[scroll-to]').on('click', function(e) {
	    var $el, id, offset;
	    e.preventDefault();
	    $el = $(this);
	    offset = $el.attr('scroll-to-offset') || -98;
	    id = $el.attr('scroll-to');
	    App.utils.scrollTo('#' + id, offset);
	    return false;
	  });
	};


/***/ },
/* 8 */
/***/ function(module, exports, __webpack_require__) {

	module.exports.init = function() {
	  return $('[data-toggle="popover"]').popover({
	    placement: function(tip, ele) {
	      var width;
	      width = $(window).width();
	      return 'top';
	    }
	  });
	};


/***/ },
/* 9 */
/***/ function(module, exports, __webpack_require__) {

	var simpleValidatorSettings;

	simpleValidatorSettings = {
	  ignore: ':hidden,:disabled,.ignore',
	  errorElement: 'span',
	  errorClass: 'error',
	  highlight: function(element) {
	    return $(element).parents('.form-group').first().addClass('has-error');
	  },
	  unhighlight: function(element) {
	    return $(element).parents('.form-group').first().removeClass('has-error');
	  },
	  errorPlacement: function(error, element) {
	    var lastRelatedElement;
	    lastRelatedElement = element.parents('.form-group').first().find('input, textarea').last();
	    if (lastRelatedElement.is('[type=radio]') && lastRelatedElement.parents('.radio').length > 0) {
	      lastRelatedElement = lastRelatedElement.next('label');
	      error = $('<div>' + error[0].outerHTML + '</div>');
	    }
	    return lastRelatedElement.after(error.addClass('help-block message-error'));
	  }
	};

	module.exports.init = function() {
	  var formEl;
	  formEl = $('[form-validate]').not('.novalidate');
	  formEl.attr('novalidate', 'novalidate');
	  return formEl.submit(function() {
	    $(this).validate(simpleValidatorSettings);
	    if ($(this).valid()) {
	      return true;
	    } else {
	      return false;
	    }
	  });
	};


/***/ },
/* 10 */
/***/ function(module, exports, __webpack_require__) {

	var activityIndicatorOff, activityIndicatorOn, captionOff, captionOn, initLightbox, overlayOff, overlayOn;

	activityIndicatorOn = function() {
	  return $("<div id=\"imagelightbox-loading\"><div></div></div>").appendTo("body");
	};

	activityIndicatorOff = function() {
	  return $("#imagelightbox-loading").remove();
	};

	overlayOn = function() {
	  return $("<div id=\"imagelightbox-overlay\"></div>").appendTo("body");
	};

	overlayOff = function() {
	  return $("#imagelightbox-overlay").remove();
	};

	captionOn = function() {
	  var description;
	  description = $("a[href=\"" + $("#imagelightbox").attr("src") + "\"] img").attr("alt") || "";
	  if (description.length) {
	    if (description.length > 0) {
	      return $("<div id=\"imagelightbox-caption\">" + description + "</div>").appendTo("body");
	    }
	  }
	};

	captionOff = function() {
	  return $("#imagelightbox-caption").remove();
	};

	initLightbox = function($selector) {
	  $($selector).imagelightbox({
	    onStart: function() {
	      overlayOn();
	    },
	    onEnd: function() {
	      captionOff();
	      overlayOff();
	      activityIndicatorOff();
	    },
	    onLoadStart: function() {
	      captionOff();
	      activityIndicatorOn();
	    },
	    onLoadEnd: function() {
	      captionOn();
	      activityIndicatorOff();
	    }
	  });
	};

	module.exports = {
	  init: initLightbox
	};


/***/ },
/* 11 */
/***/ function(module, exports, __webpack_require__) {

	var $mainBlock, $subBlocks, init;

	$mainBlock = $('.mainmenu-group').first();

	$subBlocks = $('.mainmenu-group').not($mainBlock);

	init = function() {
	  var $mainBlockCloser, $mainBlockTrigger;
	  $mainBlock.addClass('is-mainblock');
	  $mainBlock.find('.mainmenu').addClass('mainmenu-mobilecontent');
	  $subBlocks.addClass('is-subblock');
	  $subBlocks.find('.mainmenu-sub').addClass('mainmenu-mobilecontent');
	  $subBlocks.find('.mainmenu > li > a').addClass('mainmenu-opener').attr('mainmenu-opener', 'mainmenu-opener');
	  $mainBlockTrigger = $("<div mainmenu-opener class='mainmenu-opener'> <a href='javascript:;'> <i class='icon icon--hamburger'></i> </a> </div>");
	  $mainBlockCloser = $("<li class='mainmenu-closer' mainmenu-closer>Close menu</li>");
	  $mainBlock.prepend($mainBlockTrigger);
	  $('.mainmenu-mobilecontent').append($mainBlockCloser);
	  $('[mainmenu-opener]').on('click', function(e) {
	    var $el, $parent;
	    e.preventDefault();
	    $el = $(this);
	    $parent = $el.parents('.mainmenu-group');
	    if ($parent.hasClass('is-opened')) {
	      $('.mainmenu-group').removeClass('is-opened');
	      $('body').removeClass('has-opened-menu');
	      return;
	    }
	    $('.mainmenu-group').removeClass('is-opened');
	    $parent.addClass('is-opened');
	    return $('body').addClass('has-opened-menu');
	  });
	  return $('body').on('click', 'header, [mainmenu-closer]', function() {
	    $('.mainmenu-group').removeClass('is-opened');
	    return $('body').removeClass('has-opened-menu');
	  });
	};

	module.exports.init = init;


/***/ },
/* 12 */
/***/ function(module, exports, __webpack_require__) {

	var defaultOptions, featureDetection, grayStyle;

	featureDetection = __webpack_require__(15);

	defaultOptions = {
	  zoom: 8,
	  scrollwheel: 8,
	  draggable: !(featureDetection.touch && $(window).outerWidth() <= 1024),
	  icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAA8CAYAAAAdUliaAAAFxElEQVRo3s2aaWwVVRTHnzv6wd1PRkUN0eJarYmaSuISI7iUD5oghBjUgCQqEtGSiEaDJhgUagttQdGUUAoIpUBpixKoDUtpiZZCF0otVChdtO1DHn2zH8+5b+b18ZzpLO/OtJP8v7Rv7vxm5n+We++EostvD6WgS1HpqPmodag6VBcqjBJRA6hWVCXqG9RU1A2pXNPriRNRy3Q4cCkFtRM1DXW538D0NLejNA+gZupAzdHfFFfg61GFKJUTaLJ+R2XwAs5EdfoEmigJtQB1SSrAb+oDQYBai7rSC/DCgEETVWEFbQU7bxRhDW0zs4cZ7EscswAIP00CtbcRhDVPejn/Ezvg21CDbgYVN78OQsGD5rBrngAt0gt0SL8sAI85+6mRgMvdDCjtmA2gSAg9zfT/cn0+xA4NxLJZXt9SM+oqM+CpbgdTu+oYjnxwuen/lZYyZFVB2vtZqtb6MBmYKk2j24FAlUETwpaWkGtzQGkt4xELPahrEoFf9DKQFukD5chaa8v8+jHe0DmI5oznAT07EXirl0HkQ7noYRGEHzPNg+6Hx9ESCghFT/MA3m8AX+u1mgmr0lnQKW07EepZkCrfA6n6c1COlrC/KU0/A4jnQe05AtLuhRh4b3hNb4ZuJeAsTyfn3MlSmvZvF7g9tHOnQT7wLUTzJri97kwCXur8hDtA3JAFSkMRqGcOgdrXBNrQP9ZgkR6W0qwO5dgGt8CFIb2Zti8QW2ZgxTqKr7cB1NMHWTDFqBTMa1FQmjez3whFz4BQmM7eALNNwUOgdv8B0p5FIFd/gb/bwp4wOxUzjMuArCHgNvvgygMtfAq0gT/1J6aB+tc+kCrehejKNOZVgrXMFuVzQch/YPjm170Qt5LLgOwM6Tlu5HyLgZV4qJ01MYjVGfGn6OgtFU8GtX0XaNFBJsow0RVpboDDIX3CODIw82OvhU970c+1+JQ3gXx4Nat68uFV7NWrJ/cyO2j97ag2rIz1aKtjrOAYdqJcTr+P5t7tCDrkZCJJwUPBRj2DcqJq+IIWB0Ep7VWxoCQrITADNQJQP1873x0/R64vcALcT8DVtj1Dx25WteJ/w6chFk8B+bfFGOkbmUW0v1tiAFLE4k306eNk401XxLIE3hQFHvt/X7MT4HoC/tI26PYtYZnBTfoT8u9nYrk2756Ecp0dvwnyNGUTCmB66g7GLgjp/aZNsEyJXWD72ymVV7F05rCdMJClyvexoyuNWYUC0H6M1wj4CvKGrY/DnRjZA55Lq1iSNexZzNvkbWo9ySqMH4PUZowhaiOM5qfQUaPDAqrRsp00L+Hj8UnOQ//uYUHIZh9V8zEzFOrmVkE5vgOi+ffZjVWc2K09anthHDBeoYb6WTERS16B6Hd3mTdGeFNyzWLW5JOMQsHgjN+tuBcLz0SnN/9c8ozjgJMJpTZ4MimHyawCUs5VWrex/kDevxRhv2J5mb16o3do2XpRALrQCWM5KxHYWdeGlUmuW4nv9cL/c5c8hD49e3GexldO6Uzc9GoqATvHbE5HawBNjgdBi0hVH2DfW84CMl6+qf/F8ksTUPIq9cwpNu7dqKutZs3Tx8ACSrKyR5rmk08axhDsWWPyabfyM1aA5zpdW6sZA7BtelFzBJzu4+K1U73sdrn1+1GE3eVlffgWtwuDHFfi07yuwL8zCsBLUtkyoDRXGyBsR3Ia87IpQwEoBwQ8mde219cBwBbz3KcbhzruI2wv6mbeO6GZPPc+kqc+fm3dLvMBdqOfe83jXLWg9jqDutHv3fxHOO2Okr2eD+rzg0UcgHOD/F7ishQLSnPiLCIIYNIEVMQDrKjP0kNBAxu7/W6BPxqNT2gStd4FbLnd9xBBANMuVLsD2C6n1cxvYNJjujdH2uSexONavIBD+ucvVsCf8roOT2DyZqkJbKWbr6aCBCZdl+TnU6ibeF6DNzDpYdQF3dMZvMf3A5g0C/WWH2P/B0WW1s0Fws7cAAAAAElFTkSuQmCC'
	};

	grayStyle = [
	  {
	    featureType: "all",
	    elementType: "all",
	    stylers: [
	      {
	        saturation: -100
	      }
	    ]
	  }
	];

	module.exports.init = function() {
	  if (!window.google) {
	    return;
	  }
	  return $('[map]').each(function() {
	    var _center, _combinedOptions, _coords, _el, _infowindow, _map, _marker, _markerContent, _noMarker, _scrollWheel, _ui, _zoom;
	    _el = $(this);
	    _zoom = _el.attr('map-zoom') || defaultOptions.zoom;
	    _ui = (_el.attr('map-ui') === 'true') || false;
	    _noMarker = (_el.attr('map-nomarker') === 'true') || false;
	    _scrollWheel = _el.attr('map-scroll') === 'true';
	    _markerContent = _el.html();
	    _coords = _el.attr('map-location').split(',');
	    _center = new google.maps.LatLng(_coords[0], _coords[1]);
	    _combinedOptions = $.extend({}, defaultOptions, {
	      center: _center,
	      zoom: ~~_zoom,
	      scrollwheel: _scrollWheel,
	      disableDefaultUI: _ui
	    });
	    _map = new google.maps.Map(this, _combinedOptions);
	    if (!_noMarker) {
	      _marker = new google.maps.Marker({
	        position: _center,
	        map: _map,
	        icon: defaultOptions.icon,
	        title: ''
	      });
	      if (_markerContent) {
	        _infowindow = new google.maps.InfoWindow({
	          content: _markerContent,
	          maxWidth: 300
	        });
	        google.maps.event.addListener(_marker, 'click', function() {
	          return _infowindow.open(_map, _marker);
	        });
	      }
	    }
	    return setTimeout(function() {
	      google.maps.event.trigger(_map, 'resize');
	      _map.setZoom(_map.getZoom());
	      return console.log('resized', _map);
	    }, 2000);
	  });
	};


/***/ },
/* 13 */
/***/ function(module, exports, __webpack_require__) {

	module.exports.init = function() {
	  var $header, $topbar, alterHeight, checkFix, header, triggerHeight;
	  $topbar = $('.topbar');
	  $header = $('.header');
	  alterHeight = triggerHeight = 130;
	  checkFix = function() {
	    var currentScrollTop;
	    currentScrollTop = $(window).scrollTop();
	    if ((currentScrollTop > triggerHeight) && ($(window).width() >= 992)) {
	      if (!$topbar.hasClass('is-fixed')) {
	        $topbar.addClass('is-fixed is-bright');
	        return $header.addClass('is-bright');
	      }
	    } else {
	      $topbar.removeClass('is-fixed is-bright');
	      return $header.removeClass('is-bright');
	    }
	  };
	  $(window).on('scroll', checkFix);
	  $(window).on('resize', checkFix);
	  checkFix();
	  header = $('header.header');
	  return $('<img>').attr('src', header.attr('background')).imagesLoaded(function() {
	    header.removeClass('is-loading');
	    return setTimeout(function() {
	      return header.addClass('is-afterLoad');
	    }, 1300);
	  });
	};


/***/ },
/* 14 */
/***/ function(module, exports, __webpack_require__) {

	var baseUrl;

	baseUrl = window.location.host;

	module.exports.attachListener = function() {
	  return $("body").on('click', "a[href^='http:']:not([href*='" + baseUrl + "']), " + "a[href^='https:']:not([href*='" + baseUrl + "']), " + "a[href$='.pdf']:not([href*='" + baseUrl + "']), " + "a[href$='.pdf']" + "a.external", function() {
	    return $(this).attr('target', '_blank');
	  });
	};


/***/ },
/* 15 */
/***/ function(module, exports, __webpack_require__) {

	var browserDetection;

	browserDetection = {
	  init: function() {
	    this.browser = this.searchString(this.dataBrowser) || 'An unknown browser';
	    this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || 'an unknown version';
	    this.OS = this.searchString(this.dataOS) || 'an unknown OS';
	  },
	  searchString: function(data) {
	    var dataProp, dataString, i;
	    i = 0;
	    while (i < data.length) {
	      dataString = data[i].string;
	      dataProp = data[i].prop;
	      this.versionSearchString = data[i].versionSearch || data[i].identity;
	      if (dataString) {
	        if (dataString.indexOf(data[i].subString) !== -1) {
	          return data[i].identity;
	        }
	      } else if (dataProp) {
	        return data[i].identity;
	      }
	      i++;
	    }
	  },
	  isRetina: function() {
	    return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx), only screen and (min-resolution: 75.6dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min--moz-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)').matches)) || (window.devicePixelRatio && window.devicePixelRatio >= 2)) && /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
	  },
	  searchVersion: function(dataString) {
	    var index;
	    index = dataString.indexOf(this.versionSearchString);
	    if (index === -1) {
	      return;
	    }
	    return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
	  },
	  dataBrowser: [
	    {
	      string: navigator.userAgent,
	      subString: 'Chrome',
	      identity: 'Chrome'
	    }, {
	      string: navigator.userAgent,
	      subString: 'OmniWeb',
	      versionSearch: 'OmniWeb/',
	      identity: 'OmniWeb'
	    }, {
	      string: navigator.vendor,
	      subString: 'Apple',
	      identity: 'Safari',
	      versionSearch: 'Version'
	    }, {
	      prop: window.opera,
	      identity: 'Opera'
	    }, {
	      string: navigator.vendor,
	      subString: 'iCab',
	      identity: 'iCab'
	    }, {
	      string: navigator.vendor,
	      subString: 'KDE',
	      identity: 'Konqueror'
	    }, {
	      string: navigator.userAgent,
	      subString: 'Firefox',
	      identity: 'Firefox'
	    }, {
	      string: navigator.vendor,
	      subString: 'Camino',
	      identity: 'Camino'
	    }, {
	      string: navigator.userAgent,
	      subString: 'Netscape',
	      identity: 'Netscape'
	    }, {
	      string: navigator.userAgent,
	      subString: 'MSIE',
	      identity: 'Explorer',
	      versionSearch: 'MSIE'
	    }, {
	      string: navigator.userAgent,
	      subString: 'Gecko',
	      identity: 'Mozilla',
	      versionSearch: 'rv'
	    }, {
	      string: navigator.userAgent,
	      subString: 'Mozilla',
	      identity: 'Netscape',
	      versionSearch: 'Mozilla'
	    }
	  ],
	  dataOS: [
	    {
	      string: navigator.platform,
	      subString: 'Win',
	      identity: 'Windows'
	    }, {
	      string: navigator.platform,
	      subString: 'Mac',
	      identity: 'Mac'
	    }, {
	      string: navigator.userAgent,
	      subString: 'iPhone',
	      identity: 'iPhone-iPod'
	    }, {
	      string: navigator.userAgent,
	      subString: 'iPad',
	      identity: 'iPhone-iPod'
	    }, {
	      string: navigator.platform,
	      subString: 'Linux',
	      identity: 'Linux'
	    }
	  ],
	  isIE: function() {
	    return (navigator.appName === 'Microsoft Internet Explorer') || ((navigator.appName === 'Netscape') && (new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})").exec(navigator.userAgent) !== null));
	  }
	};

	module.exports = {
	  attachTo: function(_var) {
	    if (_var.client == null) {
	      _var.client = {};
	    }
	    $.extend(_var.client, {
	      os: browserDetection.OS,
	      browser: browserDetection.browser,
	      isIE: browserDetection.isIE
	    });
	    return this;
	  },
	  init: function() {
	    browserDetection.init();
	    return this;
	  },
	  addClassesOn: function() {
	    var $target;
	    $target = $(arguments[0] || '').addClass([browserDetection.OS, browserDetection.browser].join(' '));
	    if (browserDetection.isIE()) {
	      $target.removeClass('Mozilla');
	      $target.addClass('Explorer');
	    }
	    if (!!navigator.userAgent.match(/Trident.*rv[ :]*11\./)) {
	      $target.addClass('ie-11');
	    }
	    if (eval("/*@cc_on!@*/false") && document.documentMode === 10) {
	      $target.addClass('ie-10');
	    }
	    if (/MSIE\s/.test(navigator.userAgent) && parseFloat(navigator.appVersion.split("MSIE")[1]) < 10) {
	      $target.addClass('ie-9');
	    }
	    if (browserDetection.isRetina()) {
	      $target.addClass('Retina');
	    }
	    return this;
	  }
	};


/***/ },
/* 16 */
/***/ function(module, exports, __webpack_require__) {

	$.extend($.validator.messages, {
	  required: "Ce champ est obligatoire.",
	  remote: "Veuillez corriger ce champ.",
	  email: "Veuillez fournir une adresse électronique valide.",
	  url: "Veuillez fournir une adresse URL valide.",
	  date: "Veuillez fournir une date valide.",
	  dateISO: "Veuillez fournir une date valide (ISO).",
	  number: "Veuillez fournir un numéro valide.",
	  digits: "Veuillez fournir seulement des chiffres.",
	  creditcard: "Veuillez fournir un numéro de carte de crédit valide.",
	  equalTo: "Veuillez fournir encore la même valeur.",
	  extension: "Veuillez fournir une valeur avec une extension valide.",
	  maxlength: $.validator.format("Veuillez fournir au plus {0} caractères."),
	  minlength: $.validator.format("Veuillez fournir au moins {0} caractères."),
	  rangelength: $.validator.format("Veuillez fournir une valeur qui contient entre {0} et {1} caractères."),
	  range: $.validator.format("Veuillez fournir une valeur entre {0} et {1}."),
	  max: $.validator.format("Veuillez fournir une valeur inférieure ou égale à {0}."),
	  min: $.validator.format("Veuillez fournir une valeur supérieure ou égale à {0}."),
	  maxWords: $.validator.format("Veuillez fournir au plus {0} mots."),
	  minWords: $.validator.format("Veuillez fournir au moins {0} mots."),
	  rangeWords: $.validator.format("Veuillez fournir entre {0} et {1} mots."),
	  letterswithbasicpunc: "Veuillez fournir seulement des lettres et des signes de ponctuation.",
	  alphanumeric: "Veuillez fournir seulement des lettres, nombres, espaces et soulignages.",
	  lettersonly: "Veuillez fournir seulement des lettres.",
	  nowhitespace: "Veuillez ne pas inscrire d'espaces blancs.",
	  ziprange: "Veuillez fournir un code postal entre 902xx-xxxx et 905-xx-xxxx.",
	  integer: "Veuillez fournir un nombre non décimal qui est positif ou négatif.",
	  vinUS: "Veuillez fournir un numéro d'identification du véhicule (VIN).",
	  dateITA: "Veuillez fournir une date valide.",
	  time: "Veuillez fournir une heure valide entre 00:00 et 23:59.",
	  phoneUS: "Veuillez fournir un numéro de téléphone valide.",
	  phoneUK: "Veuillez fournir un numéro de téléphone valide.",
	  mobileUK: "Veuillez fournir un numéro de téléphone mobile valide.",
	  strippedminlength: $.validator.format("Veuillez fournir au moins {0} caractères."),
	  email2: "Veuillez fournir une adresse électronique valide.",
	  url2: "Veuillez fournir une adresse URL valide.",
	  creditcardtypes: "Veuillez fournir un numéro de carte de crédit valide.",
	  ipv4: "Veuillez fournir une adresse IP v4 valide.",
	  ipv6: "Veuillez fournir une adresse IP v6 valide.",
	  require_from_group: "Veuillez fournir au moins {0} de ces champs.",
	  nifES: "Veuillez fournir un numéro NIF valide.",
	  nieES: "Veuillez fournir un numéro NIE valide.",
	  cifES: "Veuillez fournir un numéro CIF valide.",
	  postalCodeCA: "Veuillez fournir un code postal valide."
	});


/***/ }
/******/ ]);