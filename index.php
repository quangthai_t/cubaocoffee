<!DOCTYPE html>
<html lang="vi" prefix="" class="no-js">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>VietSam - Better Coffee, better health</title>
	<?php 
		include 'base/header.html';
	?>
</head>
<body class="is-loading l-index">
	<div id="ajax_overlay" class="ty-ajax-overlay"></div>
	<div id="ajax_loading_box" class="ty-ajax-loading-box"></div>
	<div class="cm-notification-container notification-container"></div>
	<?php 
        include 'base/menu.html';
    ?>
	<header background="<?php echo $home_url;?>/images/thumbnails/background/homepage.png" class="header header--full is-loading">
		<div class="alignbox">
			<div class="alignbox-item alignbox-item--middle u-ta-center"><img header-hero src="" alt="home-hero" class="header-image u-fit">
					<div class="header-buttonbar">
						<a href="<?php echo $home_url;?>/introduces/company" class="button button--transparent-white button--hover-orange uppercase">
							<span data-message="text.home.slider.discover">Discover Vietruss</span>
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</a>
						<a href="<?php echo $home_url;?>/recruitment" class="button button--transparent-white button--hover-orange uppercase">
							<span data-message="text.home.slider.recruitment">Go to Recruitment</span>
						<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="header-bottom"><a href="javascript:void(0)" class="header-bottom-scroll"></a></div>
	</header>
	<div class=" no-container">
		<section class="block">
			<a href="javascript:void(0)" background class="block-side block-side--pull block-side--image">
				<img src="<?php echo $home_url;?>/images/company/DSC_0230.JPG" alt="image">
			</a>
			<div class="block-side block-side--push">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1><a href="javascript:void(0)" data-message="text.home.container.aboutUs"><span>Giới thiệu</span>Công Ty VIETRUSS</a> </h1>
							<hr class="separator">
						</header>
						<div class="content" data-message="text.home.container.aboutUs.content">
							<p>Công ty TNHH Việt Sam (Việt Sam) là cái tên rất mới trên thị trường phân phối hàng hóa tại CHLB Nga nhưng nếu xem lại lịch sử nghiên cứu và tìm hiểu thị trường tại Nga của Công ty cổ phần Đầu tư và Phát triển Việt Nga, Công ty cổ phần Thương mại và dịch vụ Quốc tế Lưu Gia (là 02 Công ty liên kết chặt chẽ với Việt Sam) thì có thể thấy các hoạt động xúc tiến thương mại của Công ty là rất lâu và hoạt động trong khá nhiều lĩnh vực.</p>
							<p>Từ mùa đông năm 2009, trong chuyến công tác lên thành phố Buôn Ma Thuột, thủ phủ của vùng đất Tây Nguyên, thuộc tỉnh Đắc lắc của Việt nam, các thành viên trong ban lãnh đạo Công ty Việt Nga, Lưu Gia ngày ấy (bây giờ là Việt Sam) đã bị mê hoặc bởi một thứ thức uống màu đen đậm, sánh sánh có vị thơm ngào ngạt mà người dân nơi đây gọi là Cà phê chồn Ban Mê.</p>
						</div>
						<footer><a href="<?php echo $home_url;?>/introduces/company" class="article-cta"><p data-message="text.home.container.more"></p>
						</a></footer>
					</article>
				</div>
			</div>
		</section>
		<section class="block">
			<a href="javascript:void(0)" background class="block-side block-side--image">
					<img src="<?php echo $home_url;?>/images/thumbnails/800/532/promo/1/block-home-1.png" alt="image">
				</a>
			<div class="block-side">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1><a href="javascript:void(0)" data-message="text.home.container.aboutHealth"><span>Sức khỏe</span>CÔNG DỤNG VÀ LỢI ÍCH</a> </h1>
							<hr class="separator">
						</header>
						<div class="content" data-message="text.home.container.aboutHealth.content">
							<p>Uống cà phê sáng đúng cách mang lại rất nhiều tác dụng, không chỉ riêng việc uống cà phê, chỉ cần ngửi mùi cà phê thôi cũng đã có thể gây những ảnh hưởng tích cực lên tâm trạng của bạn rồi. Các nhà khoa học đã nghiên cứu và chỉ ra rất nhiều tác dụng của việc uống cà phê buổi sáng.</p>
							<p>Cà phê là hoạt chất hiệu quả để chống suy nhược thần kinh. Nhiều nhà khoa học đã chứng minh cà phê có tác dụng ngăn chặn ung thư đường ruột, bởi trong cà phê có chất trung hòa độc tố trong khung đường ruột. Các nhà nghiên cứu ở Canada đã chứng minh tác dụng phong bế tế bào ung thư của cà phe trong mô hình thực nghiệm.</p>
						</div>
						<footer>
							<a href="<?php echo $home_url;?>/introduces/value-benefit" class="article-cta">
								<p data-message="text.home.container.aboutHealth.more">KHÁM PHÁ LỢI ÍCH<p>
							</a>
						</footer>
					</article>
				</div>
			</div>
		</section>
		<section vide-home style="min-height: 400px;" __data-vide-bg="<?php echo $home_url;?>/design/themes/boreal/assets/dist/videos/latte" __data-vide-options="posterType: jpg, loop: true, muted: false, position: 0% 0%" class="block bg">
			<div class="block-side hidden-xs">
			</div>
			<div class="block-side article--bright">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1><a href="<?php echo $home_url;?>/cafe/formulation" data-message="text.home.container.findPreparation"><span>Tìm Hiểu</span>Pha chế với Vietruss?</a> </h1>
							<hr class="separator">
						</header>
						<div class="content">
							<p data-message="text.home.container.findPreparation.content">
								Cách pha chế cà phê với Vietruss? Cho dù bạn đang tìm kiếm một ly cà phê phong phú của cà phê đen vào buổi sáng hoặc một ly cà phê sau bữa ăn trưa, hãy học cách pha một tách cà phê hoàn hảo cho nhu cầu và khẩu vị của bạn
							</p>
						</div>
						<!-- <div class="col-md-6 article-footer-frame is-last has-link"> -->
							<a href="<?php echo $home_url;?>/cafe/formulation" class="article-cta article-cta--dark">
								<p data-message="text.home.container.findPreparation.flowMore">HƯỚNG DẪN TÔI CÁCH PHA CHẾ<p>
							</a>
						<!-- </div> -->
					</article>
				</div>
			</div>
		</section>
	</div>
	<?php 
		include 'base/footer.html';
	?>
</body>

</html>