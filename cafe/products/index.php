<!DOCTYPE html>
<!--[if lt IE 7]>
<html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7">
   <![endif]-->
   <!--[if (IE 7)&!(IEMobile)]>
   <html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8">
      <![endif]-->
      <!--[if (IE 8)&!(IEMobile)]>
      <html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9" data-placeholder-focus="false">
         <![endif]-->
         <!--[if gt IE 8]><!-->
         <html lang="en" prefix="og: http://ogp.me/ns#" class="no-js">
            <!--<![endif]-->
            <head>
               <meta charset="utf-8">
               <meta http-equiv="X-UA-Compatible" content="IE=edge">
               <title>Introduce about products - VietSam Cafe</title>
               <!-- <base href="" /> -->
               <?php include '../../base/header.html';?>
               <style type="text/css">
                  [data-mh="cafe-galmap"],
                  [data-mh="roast-gallery"] {
                  background: #f78f1e;
                  }
               </style>
            </head>
            <body class="is-loading l-caterings">
               <!--[if lt IE 8]>
               <p class="m-dopamine m-dopamine-oldies">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
               <![endif]-->
               <!-- TOPBAR-->
               <div id="ajax_overlay" class="ty-ajax-overlay"></div>
               <div id="ajax_loading_box" class="ty-ajax-loading-box"></div>
               <div class="cm-notification-container notification-container">
               </div>
               <?php include '../../base/menu.html';?>
               <header background="<?php echo $home_url;?>/images/thumbnails/background/sanpham.png" class="header header--tall header--faded is-loading">
                  <div class="alignbox">
                     <div class="alignbox">
                        <div class="alignbox-item alignbox-item--middle u-ta-center">
                           <div class="header-suptitle" data-message="text.introduce.product.header1">Quy Trình Chế Biến, Công Thức và Các Sản Phẩm</div>
                           <div class="header-title" data-message="text.introduce.product.header2">Cùng nhau tìm hiểu quy trình chế biến và các sản phẩm cà phê của chúng tôi</div>
                        </div>
                     </div>
                  </div>
               </header>
               <div class="no-container">
                  <div class="row">
                     <section class="block">
                        <div class="col-md-6 col-sm-12" style="background-color: #f78f1e">
                           <div data-mh="cafe-galmap">
                              <div class="alignbox">
                                 <div class="alignbox-item alignbox-item--middle">
                                    <div class="article article--orange">
                                       <header>
                                          <span data-message="text.introduction">Giới thiệu</span>
                                          <h1 data-message="text.home.introduce.product.processing.title">Quy trình chế biến cà phê</h1>
                                          <hr class="separator separator--white">
                                       </header>
                                       <div class="content" data-message="text.home.introduce.product.processing.content">
                                          <p>Từ những hạt cà phê nguyên chất được trồng và kiểm soát nguồn gốc chất lượng bởi tổ chức quốc tế UTZ, cà phê nguyên liệu sẽ được đưa vào quá trình xử lý ủ và rang xay thành bột cà phê nguyên liệu.
                                          </p>
                                          <p>Trong suốt quá trình chế biến, các sản phẩm phải tuyệt đối tuân thủ nguyên tắc về vệ sinh – an toàn thực phẩm.</p>
                                          <p>Các nguyên vật liệu chế biến ra sản phẩm mà Công ty sử dụng phải đảm bảo truy xuất rõ nguồn gốc COA của sản phẩm, tuyệt đối không sử dụng các chất cấm, gây độc hại cho người tiêu dùng. </p>
                                          <p>Các sản phẩm КОФЕ УМНЫЙ đều được kiểm tra gắt gao bởi Viện vệ sinh dịch tễ trung ương tại Việt nam, cũng như tổ chức quốc tế UTZ, cơ quan kiểm tra và công bố sản phẩm EAC của CHLB Nga</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                           <div data-mh="cafe-galmap">
                              <div data-mh="cafe-galmap">
                                 <div class="gallery clearfix">
                                    <a data-imagelightbox="cafe-gallery" href="<?php echo $home_url; ?>/images/sanpham/certificate_1.png" background sizeup class="gallery-item-2"><img src="<?php echo $home_url; ?>/images/sanpham/certificate_1.png" alt="image"></a>
                                    <a data-imagelightbox="cafe-gallery" href="<?php echo $home_url; ?>/images/sanpham/certificate_2.png" background _sizeup class="gallery-item-2"><img src="<?php echo $home_url; ?>/images/sanpham/certificate_2.png" alt="image"></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                  </div>
                  <div class="container">
                     <div class="row">
                        <section class="titlebar titlebar--cup" style="padding-bottom: 0em">
                           <i class="titlebar-icon"></i>
                           <h3 class="titlebar-title" data-message="text.home.introduce.product.recipe.title">Công Thức Và Sản Phẩm</h3>
                           <div align="center">
                              <hr class="separator_long">
                           </div>
                           <br><br>
                           <p style="text-align: left" data-message="text.home.introduce.product.recipe.content1">Bột cà phê nguyên liệu sẽ được xử lý triết tách caffeine dưới dạng tinh thể nguyên chất có cấu tạo hóa học như sau:</p>
                           <img src="<?php echo $home_url;?>/images/sanpham/2.jpg" alt="" width="20%">
                           <p style="text-align: left" data-message="text.home.introduce.product.recipe.content2">
                           <div class="grid-list">
                              <div class="ty-column4">
                                 <div class="ty-grid-list__item ty-quick-view-button__wrapper">
                                    <div class="ty-grid-list__image">
                                       <a href="javascript:void(0)">
                                       <img class="ty-pict" id="det_img_27" src="<?php echo $home_url;?>/images/sanpham/4.jpg" alt="" title="">
                                       </a>
                                    </div>
                                    <div class="ty-grid-list__item-name">
                                       <a href="javascript:void(0)" class="product-title" title="Kochere, Yirgacheffe, Ethiopia">КОФЕ УМНЫЙ 3in1</a>
                                    </div>
                                 </div>
                              </div>
                              <div class="ty-column4">
                                 <div class="ty-grid-list__item ty-quick-view-button__wrapper">
                                    <div class="ty-grid-list__image">
                                       <a href="javascript:void(0)">
                                       <img class="ty-pict" id="det_img_27" src="<?php echo $home_url;?>/images/sanpham/5.png" alt="" title="">
                                       </a>
                                    </div>
                                    <div class="ty-grid-list__item-name">
                                       <a href="javascript:void(0)" class="product-title" title="Kochere, Yirgacheffe, Ethiopia">КОФЕ УМНЫЙ 2in1 </a>
                                    </div>
                                 </div>
                              </div>
                              <div class="ty-column4">
                                 <div class="ty-grid-list__item ty-quick-view-button__wrapper">
                                    <div class="ty-grid-list__image">
                                       <a href="javascript:void(0)">
                                       <img class="ty-pict" id="det_img_27" src="<?php echo $home_url;?>/images/sanpham/6.jpg" alt="" title="">
                                       </a>
                                    </div>
                                    <div class="ty-grid-list__item-name">
                                       <a href="javascript:void(0)" class="product-title" title="Kochere, Yirgacheffe, Ethiopia">КОФЕ УМНЫЙ 1in1 </a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </section>
                     </div>
                  </div>
                  <section class="mosaic clearfix">
                     <div class="mosaic-col mosaic-col--wide" style="height: 608px;">
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 607px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/3in1/IMG_E0741.png&quot;);"></a>
                     </div>
                     <div class="mosaic-col" style="height: 608px;">
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/3in1/IMG_E0785.png&quot;);"></a>
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/3in1/IMG_E0802.png&quot;);"></a>
                     </div>
                     <div class="mosaic-col" style="height: 608px;">
        <a sizeup="1:2" mosaic-item="" class="mosaic-item mosaic-item--tall bg" style="height: 608px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/3in1/IMG_E0742.png&quot;);"></a>
    </div>
                     <div class="mosaic-col" style="height: 608px;">
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/3in1/IMG_E0774.png&quot;);"></a>
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/3in1/IMG_E0802.png&quot;);"></a>
                     </div>
                     <div class="mosaic-col" style="height: 608px;">
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/2in1/IMG_E0886.png&quot;);"></a>
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/2in1/IMG_E0890.png&quot;);"></a>
                     </div>
                     <div class="mosaic-col mosaic-col--wide" style="height: 608px;">
        <a sizeup="2:1" class="mosaic-item bg bg--blue" mosaic-item="" style="background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/2in1/IMG_E0895.png&quot;); height: 304px;">
        </a>
        <a sizeup="" mosaic-item="" class="mosaic-item mosaic-item--half" href="https://www.instagram.com/p/8seKoWrAf9/" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/2in1/IMG_E0863.png&quot;);"></a>
        <a sizeup="" mosaic-item="" class="mosaic-item mosaic-item--half" href="https://www.instagram.com/p/8LGEQ6CX_R/" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/2in1/IMG_E0893.png&quot;);"></a>
    </div>
                  <div class="mosaic-col mosaic-col--wide" style="height: 608px;">
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 607px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/2in1/IMG_E0861.png&quot;);"></a>
                     </div>
                      <div class="mosaic-col" style="height: 608px;">
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/1in1/IMG_E0871.png&quot;);"></a>
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/1in1/IMG_E0876.png&quot;);"></a>
                     </div>
                     <div class="mosaic-col mosaic-col--wide" style="height: 608px;">
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 607px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/1in1/IMG_E0872.png&quot;);"></a>
                     </div>
                      <div class="mosaic-col" style="height: 608px;">
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/1in1/IMG_E0877.png&quot;);"></a>
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/1in1/IMG_E0878.png&quot;);"></a>
                     </div>
                      <div class="mosaic-col" style="height: 608px;">
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/1in1/IMG_E0882.png&quot;);"></a>
                        <a sizeup="" mosaic-item="" class="mosaic-item" style="height: 304px; background-image: url(&quot;<?php echo $home_url;?>/images/sanpham/1in1/IMG_E0883.png&quot;);"></a>
                     </div>
                  </section>
               </div>
               <?php 
                  include '../../base/footer.html';
                  ?>
            </body>
         </html>