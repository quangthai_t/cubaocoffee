<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9" data-placeholder-focus="false"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" prefix="og: http://ogp.me/ns#" class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Fomulation - VietSam Cafe</title>
	<link type="text/plain" rel="author" href="/humans.txt">
	<!-- <base href="" /> -->
	<?php include '../../base/header.html';?>
	<script type="text/javascript" data-no-defer>
		window.jsErrors = [];
		window.onerror = function(errorMessage) {
			document.write('<div data-ca-debug="1" style="border: 2px solid red; margin: 2px;">' + errorMessage + '</div>');
		}
	</script>
</head>

<body class="is-loading l-caterings">
	<!-- TOPBAR-->
	<div id="ajax_overlay" class="ty-ajax-overlay"></div>
	<div id="ajax_loading_box" class="ty-ajax-loading-box"></div>
	<div class="cm-notification-container notification-container">
	</div>
	<?php include '../../base/menu.html'; ?>
	<header background="<?php echo $home_url;?>/images/thumbnails/background/congthucphache.png" class="header header--tall header--faded is-loading">
		<div class="alignbox">
			<div class="alignbox-item alignbox-item--middle u-ta-center">
				<div class="header-suptitle" data-message="text.home.introduce.product.formulation.header-title.title1">Giới Thiệu Cách Pha Chế</div>
				<div class="header-title" data-message="text.home.introduce.product.formulation.header-title.title2">Các cách pha chế từ КОФЕ УМНЫЙ - Cà phê Việt Nam</div>
		</div>
	</header>
	<div class="no-container">
		<section class="block">
			<div class="container">
				<div class="row">
					<section class="titlebar titlebar--cup">
						<i class="titlebar-icon"></i>
						<h3 class="titlebar-title" data-message="text.home.introduce.product.formulation.header.title">Lời Mở Đầu</h3>
						<div align="center">
							<hr class="separator_long">
						</div>
						<br><br>
						<p style="text-align: left" data-message="text.home.introduce.product.formulation.header.content">Không chỉ đơn giản là một loại thức uống, thưởng thức cà phê cùng là một nghệ thuật. Chút phá cách trong việc kết hợp nguyên liệu tinh tế trên nền cà phê truyền thống tạo nên điểm nhấn độc đáo trong nét văn hóa của các quốc gia trên thế giới. Sau đây là 7 công thức pha chế cà phê lừng danh thế giới, cà phê trứng của Việt Nam cũng là một trong số đó.</p>
					</section>
				</div>
			</div>
		</section>

		<section class="block">
			<div class="no-container block clearfix">
				<div class="block-side block-side--miel block-side--pull block-side--content">
					<div class="block-side-inner">
						<article class="article">
							<header>
								<h1 data-message="text.home.introduce.product.formulation.#2.title"><span>#2</span>Cà phê Miel – Tây Ban Nha</h1>
								<hr class="separator">
							</header>
							<div class="content" data-message="text.home.introduce.product.formulation.#2.content">
								<p>Cà phê Miel của Tây Ban Nha là sự hòa quyện hài hòa giữa mật ong ngọt dịu, sữa nóng, bột quế trên nền cà phê espresso truyền thống. Đối với cà phê Miel, người pha chế phải đặc biệt tuân thủ định lượng chính xác trong công thức để tạo nên hương vị cà phê Miel chuẩn vị. Chút vị ngọt ngào của mật ong kết hợpvới vị béo ngậy của sữa tươi, xen chút vị đắng đặc trưng của cà phê espresso khiến người thưởng thức lưu luyến mãi ngay từ lần đầu nếm thử. </p>
							</div>
						</article>
					</div>
				</div>
				<div class="block-side block-side--tonic block-side--push block-side--content">
					<div class="block-side-inner">
						<article class="article">
							<header>
								<h1 data-message="text.home.introduce.product.formulation.#1.title"><span>#1</span>Cà phê Kaffe Tonic – Thụy Điển</h1>
								<hr class="separator">
							</header>
							<div class="content" data-message="text.home.introduce.product.formulation.#1.content">
								<p>Nước Tonic chính là thành phần không thể thiếu trong công thức pha chế món cà phê Kaffe Tonic độc đáo này. Nước Tonic, cà phê espresso và đá lạnh được kết hợp cùng nhau theo một tỉ lệ nhất định, mang đến hương vị cà phê đậm đà, mát lạnh. Người Thụy Điển thưởng thức cà phê Kaffe Tonic trong những ngày hè oi bức, giúp cơ thể sáng khoái và thư giãn. Nước Tonic giúp cho cà phê trung hòa bớt vị đắng và trở nên dịu nhẹ hơn. </p>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>
		
		<section class="block">
			<div class="no-container block clearfix">
				<div class="block-side block-side--pull block-side--kaffeost block-side--content">
					<div class="block-side-inner">
						<article class="article">
							<header>
								<h1 data-message="text.home.introduce.product.formulation.#4.title"><span>#4</span>Cà phê phô mai Kaffeost – Phần Lan và Thụy Điển</h1>
								<hr class="separator">
							</header>
							<div class="content" data-message="text.home.introduce.product.formulation.#4.content">
								<p>Phô mai chính là nguyên liệu độc đáo trong công thức pha chế cà phê phô mai Kaffeost của người Phần Lan và Thụy Điển. Nhúng nhẹ miếng phô mai vào tách cà phê nóng hổi, phô mai từ từ tan ra, hòa quyện vào cà phê, đẩy hương thơm cà phê rất đặc trưng. Nếu có cơ hội, hãy thư giãn, nhâm nhi những tách cà phê Kaffeost hấp dẫn và chiêm ngưỡng cảnh quan kì vĩ của vùng biên giới Thụy Điển – Phần Lan. Đây cũng là thú vui thú vị của rất nhiều du khách khi đặt chân đến đây. </p>
							</div>
						</article>
					</div>
				</div>
				<div class="block-side block-side--push block-side--pharisear block-side--content">
					<div class="block-side-inner">
						<article class="article">
							<header>
								<h1 data-message="text.home.introduce.product.formulation.#3.title"><span>#3</span>Cà phê Pharisear – Đức</h1>
								<hr class="separator">
							</header>
							<div class="content" data-message="text.home.introduce.product.formulation.#3.content">
								<p>Cà phê Pharisear của Đức nổi tiếng bởi hương vị đậm đà đến từ sự kết của cà phê đậm đặc, rượu Rhum và kem tươi béo ngậy. Nhâm nhi từng ngụm nhỏ cà phê Pharisear để cảm nhận của chất men rượu rhum nồng nàn lan tỏa đến mọi giác quan, nhẹ nhàng đánh thức vị giác, đầy lôi cuốn. </p>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>
		
		<section class="block">
			<div class="no-container block clearfix">
				<div class="block-side block-side--pull block-side--brazil block-side--content">
					<div class="block-side-inner">
						<article class="article">
							<header>
								<h1 data-message="text.home.introduce.product.formulation.#6.title"><span>#6</span>Sự kết hợp giữa cà phê và nước đường đậm đặc – Brazil</h1>
								<hr class="separator">
							</header>
							<div class="content" data-message="text.home.introduce.product.formulation.#6.content">
								<p>Cafezinho chính là tên gọi của một tách cà phê đậm đà, đặc trưng cùa vùng quốc gia Nam Mỹ. Sự khác biệt của cafezinho nằm ở công thức pha chế. Đường được hòa với nước trước khi đem đun sôi, tạo nên dung dịch nước đường trong, đậm đặc và quyện sánh. Khi hòa tan cà phê với nước đường, cà phê sẽ có vị đậm đà và hương thơm rất hấp dẫn. </p>
							</div>
						</article>
					</div>
				</div>
				<div class="block-side block-side--push block-side--diepxoan block-side--content">
					<div class="block-side-inner">
						<article class="article">
							<header>
								<h1 data-message="text.home.introduce.product.formulation.#5.title"><span>#5</span>Cà phê hoa rau diếp xoắn – Ấn Độ</h1>
								<hr class="separator">
							</header>
							<div class="content" data-message="text.home.introduce.product.formulation.#5.content">
								<p>Màu tím và vị mặn của hoa rau diếp xoắn chính là điểm nhấn độc đáo trong những tách cà phê cùa số đông người Ấn Độ. Hoa rau diếp xoắn giúp làm chậm quá trình lọc cà phê qua phin, khiến cà phê thêm đậm đà và có hương thơm rất đặc biệt. Chút vị béo thơm của sữa tươi và vị ngọt dịu của đường trắng tinh luyện sẽ giúp cho cà phê thêm hấp dẫn. </p>
							</div>
						</article>
					</div>
				</div>
			</div>
		</section>
	</div>
	<?php 
        include '../../base/footer.html';
    ?>

</body>

</html>