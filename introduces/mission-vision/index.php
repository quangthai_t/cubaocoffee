<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9" data-placeholder-focus="false"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" prefix="og: http://ogp.me/ns#" class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Mission & Vision - VietSam Cafe</title>
	<link type="text/plain" rel="author" href="/humans.txt">
	<!-- <base href="" /> -->
	<?php include '../../base/header.html';?>
	<script type="text/javascript" data-no-defer>
		window.jsErrors = [];
		window.onerror = function(errorMessage) {
			document.write('<div data-ca-debug="1" style="border: 2px solid red; margin: 2px;">' + errorMessage + '</div>');
		}
	</script>
</head>

<body class="is-loading l-caterings">
	<!--[if lt IE 8]><p class="m-dopamine m-dopamine-oldies">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
	<!-- TOPBAR-->
	<div id="ajax_overlay" class="ty-ajax-overlay"></div>
	<div id="ajax_loading_box" class="ty-ajax-loading-box"></div>
	<div class="cm-notification-container notification-container">
	</div>
	<?php 
        include '../../base/menu.html';
    ?>
	<header background="<?php echo $home_url;?>/images/thumbnails/background/sumangtamnhin.png" class="header header--tall header--faded is-loading">
		<div class="alignbox">
			<div class="alignbox-item alignbox-item--middle u-ta-center">
				<div class="header-suptitle" data-message="text.introduce.mission.header1">Công ty TNHH Việt Sam</div>
				<div class="header-title" data-message="text.introduce.mission.header2">Sứ Mệnh, Tầm Nhìn và Giá Trị Cốt Lõi</div>
			</div>
		</div>
	</header>
	<div class="no-container">
		<section class="block">
			<img src="<?php echo $home_url;?>/images/mission-and-vission/1.jpg" alt="" class="block-side undefined block-side--pull block-side--image">
			<div class="block-side block-side--push" style="height: 497px;">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1 data-message="text.home.introduce.companyDetail.mission.title">
								<span></span> Sứ Mệnh </h1>
							<hr class="separator">
						</header>
						<div class="content">
							<ul data-message="text.home.introduce.companyDetail.mission.content">
								<li>Mang lại sức khỏe cho người tiêu dùng qua từng sản phẩm “sạch-chất lượng” mà công ty sàng lọc, chắt chiu, kiểm soát đến từng chi tiết</li>
								<li>Chắt chiu từng đồng lợi nhuận để dùng một phần đóng góp cho tương lai thế hệ trẻ vì sự phồn vinh của dân tộc</li>
								<li>Chia sẻ, cùng đồng hành và phát triển với tất cả các đối tác vì sự phồn vinh, giàu có của cộng đồng doanh nghiệp</li>
							</ul>
						</div>
					</article>
				</div>
			</div>
		</section>
		<section class="block">
			<img src="<?php echo $home_url;?>/images/mission-and-vission/2.jpg" alt="" class="block-side undefined block-side--image">
			<div class="block-side undefined" style="height: 434px;">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1 data-message="text.home.introduce.companyDetail.vision.title">
								<span></span>Tầm Nhìn</h1>
							<hr class="separator">
						</header>
						<div class="content">
							<ul data-message="text.home.introduce.companyDetail.vision.content">
								<li>Hội nhập – phát triển bền vững</li>
								<li>Mang sức sáng tạo lan tỏa đến mọi vùng đất trên toàn thế giới để doanh nghiệp ngày một nâng lên tầm cao mớiv
								<li>Giữ gìn bản sắc dân tộc, kết hợp với văn hóa địa phương nơi công ty kinh doanh để hoàn thiện sản phẩm mang tính dân tộc – đậm nét vắn hóa bản địa – phù hợp tập quán, văn hóa nước sở tại</li>
							</ul>
						</div>
					</article>
				</div>
			</div>
		</section>
		<section class="block">
			<img src="<?php echo $home_url;?>/images/mission-and-vission/3.jpg" alt="" class="block-side undefined block-side--pull block-side--image">
			<div class="block-side block-side--push" style="height: 497px;">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1 data-message="text.home.introduce.companyDetail.mainValue.title">
								<span></span>Giá Trị Cốt Lõi</h1>
							<hr class="separator">
						</header>
						<div class="content">
							<ul data-message="text.home.introduce.companyDetail.mainValue.content">
								<li>Mang đến những sản phẩm từ cà phê thật sự tự nhiên, sạch – chất lượng – đậm đà nét tinh hoa</li>
								<li>Những sản phẩm cà phê УМНЫЙ đánh thức những tư duy sáng tạo, làm thức giấc mọi giác quan, đưa con người đạt đến độ hưng phấn vượt mọi cảm xúc …</li>
								<li>УМНЫЙ Cà phê của chúng tôi sẽ cùng các bạn kết hợp các ý tưởng hợp tác kinh doanh, hợp tác cùng làm giàu</li>
								<li>Cùng nhau chắt chiu giọt cà phê đắng – sẻ chia những ngọt bùi, bằng tấm lòng thiện nguyện nâng bước những hoàn cảnh khó khăn, vì một xã hội tốt đẹp hơn …</li>
							</ul>
						</div>
					</article>
				</div>
			</div>
		</section>
	</div>
	<?php 
		include '../../base/footer.html';
	?>
</body>

</html>