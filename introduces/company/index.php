<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9" data-placeholder-focus="false"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" prefix="og: http://ogp.me/ns#" class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Introduce company - VietSam Cafe</title>
	<!-- <base href="" /> -->
	<meta name="description" content="Vietruss - Better Coffee, better health" />
	<?php include '../../base/header.html';?>
</head>

<body class="is-loading l-caterings">
	<!--[if lt IE 8]><p class="m-dopamine m-dopamine-oldies">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
	<!-- TOPBAR-->
	<div id="ajax_overlay" class="ty-ajax-overlay"></div>
	<div id="ajax_loading_box" class="ty-ajax-loading-box"></div>
	<div class="cm-notification-container notification-container">
	</div>
	<?php 
        include '../../base/menu.html';
    ?>
	<header background="<?php echo $home_url;?>/images/thumbnails/background/congty.png" class="header header--tall header--faded is-loading">
		<div class="alignbox">
			<div class="alignbox-item alignbox-item--middle u-ta-center">
				<div class="header-suptitle" data-message="text.introduce.company.header1">Công ty Việt Sam</div>
				<div class="header-title" data-message="text.introduce.company.header2">Giới Thiệu Công Ty và Sự Hình Thành Phát Triển</div>
			</div>
		</div>
	</header>
	<div class="no-container">
		<div class="row">
			<div class="col-md-5 col-sm-12">
				<div data-mh="roast-gallery" class="testimonial bg bg--orange">
					<div class="alignbox">
						<div class="alignbox-item alignbox-item--middle">
							<div class="article article--orange">
								<header>
									<h1 data-message="text.home.introduce.company.header.title">Công ty TNHH Việt Sam</h1>
									<hr class="separator separator--white">
								</header>
								<div class="content" data-message="text.home.introduce.company.header.content">
									<p>
										Công ty TNHH Việt Sam (Việt Sam) là cái tên rất mới trên thị trường phân phối hàng hóa tại CHLB Nga nhưng nếu xem lại lịch sử nghiên cứu và tìm hiểu thị trường tại Nga của Công ty cổ phần Đầu tư và Phát triển Việt Nga, Công ty cổ phần Thương mại và dịch vụ Quốc tế Lưu Gia (là 02 Công ty liên kết chặt chẽ với Việt Sam) thì có thể thấy các hoạt động xúc tiến thương mại của Công ty là rất lâu và hoạt động trong khá nhiều lĩnh vực.
									</p>
									<ul>
										<li>Các hoạt động quảng bá và tìm hiểu thị trường</li>
										<li>Các hoạt động xuất khẩu nông sản sang Nga</li>
										<li>Các hoạt động nhập khẩu mỹ phẩm từ Nga về Việt nam</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-7 col-sm-12">
				<div data-mh="roast-gallery">
					<div class="gallery clearfix">
						<a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/company/20151203_100720.jpg" background _sizeup class="gallery-item">
                                <img src="<?php echo $home_url;?>/images/company/20151203_100720.jpg" alt="gallery-item">
                            </a>
						<a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/company/20160818_154601.jpg" background _sizeup class="gallery-item">
                                <img src="<?php echo $home_url;?>/images/company/20160818_154601.jpg" alt="gallery-item">
                            </a>
                        <a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/company/DSC_0230.JPG" background _sizeup class="gallery-item">
                                <img src="<?php echo $home_url;?>/images/company/DSC_0230.JPG" alt="gallery-item">
                            </a>
						<a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/company/IMG_1063.JPG" background _sizeup class="gallery-item">
                                <img src="<?php echo $home_url;?>/images/company/IMG_1063.JPG" alt="gallery-item">
                            </a>
						<a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/company/IMG_1066.JPG" background _sizeup class="gallery-item">
                                <img src="<?php echo $home_url;?>/images/company/IMG_1066.JPG" alt="gallery-item">
                            </a>
						<a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/company/IMG_1068.JPG" background _sizeup class="gallery-item">
                                <img src="<?php echo $home_url;?>/images/company/IMG_1068.JPG" alt="gallery-item">
                            </a>
						<a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/company/IMG_1067.JPG" background _sizeup class="gallery-item">
                                <img src="<?php echo $home_url;?>/images/company/IMG_1067.JPG" alt="gallery-item">
                            </a>
                        <a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/company/IMG_1064.JPG" background _sizeup class="gallery-item">
                                <img src="<?php echo $home_url;?>/images/company/IMG_1064.JPG" alt="gallery-item">
                            </a>
						<a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/company/IMG_1065.JPG" background _sizeup class="gallery-item">
                                <img src="<?php echo $home_url;?>/images/company/IMG_1065.JPG" alt="gallery-item">
                            </a>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<section class="titlebar titlebar--cup">
					<i class="titlebar-icon"></i>
					<h3 class="titlebar-title" data-message="text.home.introduce.company.container.title">Khởi Nguồn Của Sự Hình Thành</h3>
					<div align="center">
						<hr class="separator_long">
					</div>
					<br><br>
					<div data-message="text.home.introduce.company.container.content">
						<p style="text-align: left">Từ mùa đông năm 2009, trong chuyến công tác lên thành phố Buôn Ma Thuột, thủ phủ của vùng đất Tây Nguyên, thuộc tỉnh Đắc lắc của Việt nam, các thành viên trong ban lãnh đạo Công ty Việt Nga, Lưu Gia ngày ấy (bây giờ là Việt Sam) đã bị mê hoặc bởi một thứ thức uống màu đen đậm, sánh sánh có vị thơm ngào ngạt mà người dân nơi đây gọi là Cà phê chồn Ban Mê.</p>
						<p style="text-align: left">Tất cả chúng ta đều không ai lạ gì cà phê, nhất là những người ở vùng nhiệt đới, nhưng ngồi giữa cái se lạnh của núi rừng Ban Mê, thưởng thức lý cà phê nóng ấm vào sáng ban mai, làm chúng tôi liên tưởng đến đang ngồi thưởng thức ly cà phê Ban Mê trong buổi sớm thu bên dòng Volga hoặc ở bên những sườn đồi của vùng Kazan thơ mộng . . .Và một thành viên chợt buột miệng thốt lên :”Ước gì được uống ly cà phê này ở nước Nga nhỉ !!!”. Và câu chuyện cà phê УМНЫЙ đã thực sự bắt đầu chuẩn bị lên đường đi Nga từ dạo đấy …</p>
						<p style="text-align: left">Từ năm 2009, tập thể ban lãnh đạo Việt Sam đã bắt tay vào nghiên cứu nhiều dòng cà phê nổi tiếng khác nhau của Việt nam và thế giới, sau bao thử nghiệm, sáng tạo, cuối cùng, Việt Sam cho ra đời dòng sản phẩm cà phê УМНЫЙ – Cà phê Việt Nam đầy chất lượng và độc đáo. 
						</p>
						<p style="text-align: left">
						УМНЫЙ – Cà phê Việt nam đã đạt được giấy chứng nhận của UTZ về chứng minh xuất xứ và truy xuất nguồn gốc cà phê sạch cùng các giấy chứng nhận về vệ sinh, an toàn thự phẩm của các tổ chức chứng nhận có uy tín tại Việt nam cũng như quốc tế.</p>
						<p style="text-align: left">
							Giờ đây, mỗi khi tuyết rơi trắng xóa ngoài kia tại nước Nga xa xôi, trong lòng chúng ta lại cảm thấy ấm áp kỳ lạ bên tách cà phê УМНЫЙ, hương thơm quyến rũ, ngào ngạt lan tỏa để xua đi nhưng mệt mỏi, những vất vả…giúp mọi người cảm thấy tỉnh táo hơn, phấn chấn hơn và hứa hẹn một thành quả công việc thật tốt hơn……hãy nhắm mắt lại, УМНЫЙ sẽ đưa bạn đến những vùng đồi cà phê nhiệt đới bạt ngàn, xanh ngút tầm mắt…lên đường thôi !!!
						</p>
					</div>
				</section>
			</div>
		</div>
	</div>
	<?php include '../../base/footer.html'; ?>
</body>

</html>