<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9" data-placeholder-focus="false"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" prefix="og: http://ogp.me/ns#" class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Values & benifit of coffee for health - VietSam Cafe</title>
	<!-- <base href="" /> -->
	<?php include '../../base/header.html';?>
</head>

<body class="is-loading l-caterings">
	<div id="ajax_overlay" class="ty-ajax-overlay"></div>
	<div id="ajax_loading_box" class="ty-ajax-loading-box"></div>
	<div class="cm-notification-container notification-container">
	</div>
	<?php 
        include '../../base/menu.html';
    ?>
	<header background="<?php echo $home_url; ?>/images/thumbnails/background/congdungloiich.png" class="header header--tall header--faded is-loading">
		
		<div class="alignbox">
            <div class="alignbox-item alignbox-item--middle u-ta-center">
                <div class="header-suptitle" data-message="text.introduce.value.header1">Công Dụng và Lợi Ích</div>
                <div class="header-title" data-message="text.introduce.value.header2">Các Công Dụng, Lợi Ích và Các Uống Của Cà Phê</div>
            </div>
        </div>

	</header>
	<div class="no-container">

		<section class="titlebar titlebar--cup">
			<i class="titlebar-icon"></i>
			<h3 class="titlebar-title" data-message="text.home.introduce.benifitForHealth">CÁC CÔNG DỤNG VÀ LỢI ÍCH CHO SỨC KHỎE CỦA CÀ PHÊ</h3>
			<div align="center">
				<hr class="separator_long">
			</div>
		</section>
		<section class="block">
			<a href="<?php echo $home_url;?>/cafes/geneva/" background class="block-side block-side--image">
					<img src="<?php echo $home_url;?>/images/thumbnails/800/532/promo/1/block-home-1.png" alt="image">
				</a>
			<div class="block-side">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1><a href="" data-message="text.home.introduce.benifitForHealth.title"><span>Tìm hiểu</span>CÔNG DỤNG KHI UỐNG CAFE</a> </h1>
							<hr class="separator">
						</header>
							<div class="content" style=" overflow-y: scroll; height: 380px" data-message="text.home.introduce.benifitForHealth.content">
							<p>Uống cà phê sáng đúng cách mang lại rất nhiều tác dụng, không chỉ riêng việc uống cà phê, chỉ cần ngửi mùi cà phê thôi cũng đã có thể gây những ảnh hưởng tích cực lên tâm trạng của bạn rồi. Các nhà khoa học đã nghiên cứu và chỉ ra rất nhiều tác dụng của việc uống cà phê buổi sáng:
							</p>
							<ul>
								<li>Cà phê chứa các chất chống oxy hoá, giúp ngăn ngừa quá trình lão hoá.</li>
								<li>Giúp giảm cân, giữ dáng, mang lại vòng eo thon nhờ khả năng  đốt cháy mỡ thừa.</li>
								<li>Mang lại tinh thần sảng khoái, tỉnh táo hơn, tập trung hơn, hạnh phúc hơn, từ đó ngăn ngừa nguy cơ mắc bệnh trầm cảm.</li>
								<li>Tăng hiệu quả luyện tập khi tập các bộ môn thể thao.</li>
								<li>Giảm nguy cơ mắc một số bệnh ung thư, điển hình là ung thư gan và ung thư cổ tử cung.</li>
								<li>Bảo vệ gan, ngừa các bệnh về gan.</li>
								<li>Giảm nguy cơ mắc chứng đột quỵ.</li>
								<li>Ngăn ngừa tổn thương võng mạc và các bệnh về mắt.</li>
								<li>Giảm và ngăn ngừa tình trạng đau đầu.</li>
								<li>Giúp ngăn ngừa hàng loạt căn bệnh như bệnh tiểu đường, đa xơ cứng (MS), Parkinson, Alzheimer...</li>
								<li>Gia tăng tuổi thọ.</li>
							</ul>
							<p>Ngay từ đầu thế kỷ 17, các thầy thuốc đã dùng cà phê như một thứ thuốc. Nhiều thầy thuốc ở Canada, Hoa Kỳ, Bắc Âu, vẫn dùng cà phê như nhân tố hỗ trợ trong phác đồ điều trị hen suyễn, viêm mũi dị ứng. Về sau, cà phê trở thành thức uống phổ thông, do tác dụng tăng cường sự hoạt động ở vùng não nhờ các hoạt chất caffein.</p>
							<p>Cà phê là hoạt chất hiệu quả để chống suy nhược thần kinh. Nhiều nhà khoa học đã chứng minh cà phê có tác dụng ngăn chặn ung thư đường ruột, bởi trong cà phê có chất trung hòa độc tố trong khung đường ruột. Các nhà nghiên cứu ở Canada đã chứng minh tác dụng phong bế tế bào ung thư của cà phe trong mô hình thực nghiệm. Cà phê đen, không đường, không sữa còn có tác dụng ổn định huyết áp, đặc biệt ở người cao tuổi có huyết áp dao động bất thường.</p>
							<p>Chuyên gia nghiên cứu Forsyth đã chứng minh cà phê có tính kháng khuẩn trong vòm miệng và ngăn ngừa hư răng nhờ lượng chất chát rất cao. Uống cà phê thật chậm hay thậm chí dùng nước giảo cà phê để súc miệng là biện pháp hữu hiệu để khỏi hư răng.</p>
						</div>
					</article>
				</div>
			</div>
		</section>
		<section class="block">
			<a href="/roasting/" background class="block-side block-side--pull block-side--image">
				<img src="<?php echo $home_url;?>/images/thumbnails/800/532/promo/1/1727_coffee-beans-perfect.jpg" alt="image">
			</a>
			<div class="block-side block-side--push">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1><a href="" data-message="text.home.introduce.benifitForHealth.using.title"><span>Tìm hiểu</span>UỐNG CÀ PHÊ THẾ NÀO MỚI ĐÚNG CÁCH</a> </h1>
							<hr class="separator">
						</header>
							<div class="content" style=" overflow-y: scroll; height: 380px" data-message="text.home.introduce.benifitForHealth.using.content">
							<p><b style="font-weight: bold; font-size: 18px">Limitations for sugar, ice cream ...</b><br> Cream, milk, syrup, sugar ... are often combined with coffee to create a more delicious taste. However, it causes calories to rise, while reducing the real effect of coffee. So, you should limit them to coffee. If possible, drinking pure coffee is better.</p>
							<p><b style="font-weight: bold; font-size: 18px">Just drink a moderate amount</b><br> Drinking coffee is good for health, but that is when we apply with the right amount of "1 - 3 cups of coffee a day is the right amount you should consume. Also, if the coffee makes you feel strange signs such as abdominal pain, insomnia or mental instability then should be reduced!
							</p>
							<p><b style="font-weight: bold; font-size: 18px">Coffee usually has the strongest effect within a few hours after drinking and its residual well remains for a long time. Therefore, you should only drink in the morning by drinking after noon will affect your sleep, especially in the evening.
							</p>
						</div>
					</article>
				</div>
			</div>
		</section>
	</div>
	<?php 
        include '../../base/footer.html';
    ?>

</body>

</html>
