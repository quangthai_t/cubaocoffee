<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9" data-placeholder-focus="false"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" prefix="og: http://ogp.me/ns#" class="no-js">
<!--<![endif]-->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Vietnamese Coffee - VietSam Cafe</title>
	<link type="text/plain" rel="author" href="/humans.txt">
	<!-- <base href="" /> -->
	<?php include '../../base/header.html';?>
	<style>


	</style>
</head>

<body class="is-loading l-caterings">
	<!--[if lt IE 8]><p class="m-dopamine m-dopamine-oldies">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
	<!-- TOPBAR-->
	<div id="ajax_overlay" class="ty-ajax-overlay"></div>
	<div id="ajax_loading_box" class="ty-ajax-loading-box"></div>
	<div class="cm-notification-container notification-container">
	</div>
	<?php 
        include '../../base/menu.html';
    ?>
	<header background="<?php echo $home_url; ?>/images/thumbnails/background/gioithieucaycafe.jpg" class="header header--tall header--faded is-loading">
		<div class="alignbox">
			<div class="alignbox-item alignbox-item--middle u-ta-center">
                <div class="header-suptitle" data-message="text.introduce.coffeTree.header1">Cây Cà Phê Việt Nam</div>
                <div class="header-title" data-message="text.introduce.coffeTree.header2">Nguồn Gốc, Lịch Sử và Hành Trình</div>
			</div>
		</div>
	</header>
	<div class="no-container">
		<section class="block">
			<div background class="block-side block-side--image">
				<img src="<?php echo $home_url; ?>/images/thumbnails/800/532/promo/1/1499754898-3313-4-216942.jpg" alt="image">
			</div>
			<div class="block-side">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1 data-message="text.home.introduce.coffeTree.source.title"><span>Nguồn gốc</span>Cây cà phê</h1>
							<hr class="separator">
						</header>
							<div class="content" style=" overflow-y: scroll; height: 380px" data-message="text.home.introduce.coffeTree.source.content">
							<p>Những câu chuyện về cà phê thì rất nhiều, thực hay hư thì cũng ít ai kiểm chứng, đôi khi họ phóng đại lên cho nó ly kỳ, thú vị như chính cái hậu vị để lại khi giọt cà phê tan vào trong từng tế bào cảm giác!
							</p>
							<p>Trong những câu chuyện đó, từ chuyện nghe có vẻ hợp lý nhất là câu chuyện về anh chàng chăn dê tên Kaldi người xứ Abyssinia với đàn dê của anh ta. Chuyện kể rằng, đàn dê của anh đã ăn một loại quả cây lạ có màu đo đỏ rồi sau đó có những biểu hiện lạ thường. Kaldi phát hiện ra điều đó, anh liều ăn thử và thấy mình hưng phấn hẳn lên, ngờ rằng mình đã gặp một phép lạ bèn báo ngay cho vị quả n nhiệm ở một tu viện gần đó. Nhà tu kia sợ rằng đây chính là một thứ trái cấm của quỷ dữ nên lập tức vứt vào lò lửa, thế nhưng khi những quả kia cháy xém tỏa ra một mùi thơm lừng, đến lúc này người tu sĩ kia mới tin rằng đó là một món quà của Thượng Đế ban tặng nên vội kêu thêm những tăng lữ khá c đến tiếp tay. Họ đem rang lên, giã nhỏ rồi pha vào nước uống để mọi người cùng hưởng thiên ân. Đến những câu chuyện về sự độc hại của cà phê, như câu chuyện ở đất nước Thụy Điển, quốc vương Gusitafu đệ tam muốn thử xem cà phê có độc hay không bèn ra quyết định bắt hai anh em tội phạm bị kết án tử hình đang giam trong ngục mỗi ngày phải được cho uống thứ nước làm từ quả ấy hai lần, thử xem họ chết ra sao? Đến lúc chết, vị hoàng đế này vẫn để lại di chỉ cho người kế vị là phải tiếp tục làm theo lệnh của ông ta, như một phép lạ, hai tử tù kia qua đời ở tuổi hơn 80! Và có lẽ đó là ghi nhận đầu tiên của loài người về tính dược lý của cà phê,…
							</p>
							<p>
								Đó là truyền thuyết, còn những di chỉ khảo cổ, những ghi chép của con người còn lại cho đến ngày nay. Người ta biết rằng, Kaffa (Ethiopia ngày nay) chính là vùng đất khởi nguyên của cây cà phê, từ thế kỷ thứ IX đã có những ghi nhận ở đây, đến thế kỷ XIV những người buôn nô lệ đã mang cà phê từ Ethiopia sang xứ Ả Rập. Nhưng tới tận thế kỷ XV người ta mới biết rang cà phê lên và sử dụng làm đ ồ uống. Cà phê đã trở thành một thức uống truyền thống của người Ả Rập và là nơi trồng cà phê độc quyền với trung tâm giao dịch cà phê là thành phố cảng Mocha, hay còn được gọi là Mokka, tức là thành phố Al Mukha thuộc Yemen ngày nay. Người Ả Rập rất tự hào về phát minh ra loại thức uống này và giữ bí mật để bảo tồn độc quyền về một loại sản phẩm. Họ đưa ra những chế tài rất chặt chẽ trong việc sản xuất và xuất khẩu cà phê như: Chỉ mang hạt ra khỏi xứ sau khi đã rang chín, người ngoại quốc cũng bị cấm không cho bén mảng đến các đồn điền cà phê. Thế nhưng dù nghiêm ngặt đến mức nào thì cũng có người vượt qua được, những khách hành hương được thưởng thức nước cà phê đã lén lút mang hạt giống về trồng, chẳng bao lâu khắp khu vực Trung Đông đều có trồng và truyền đi mỗi lúc một xa hơn.
							</p>
						</div>
					</article>
				</div>
			</div>
		</section>
		<section class="block">
			<div background class="block-side block-side--pull block-side--image">
				<img src="<?php echo $home_url; ?>/images/thumbnails/800/532/promo/1/1481991283_pole.jpg" alt="image">
			</div>
			<div class="block-side block-side--push">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1 data-message="text.home.introduce.coffeTree.journeys.title"><span>Hành trình</span>Du nhập vào châu Âu và Bắc Mỹ</h1>
							<hr class="separator">
						</header>
							<div class="content" style=" overflow-y: scroll; height: 380px" data-message="text.home.introduce.coffeTree.journeys.content">
							<p>Sau nhiều lần thất bại, người Hà Lan là dân tộc đầu tiên ở Châu Âu lấy được hạt giống cây này mang về thử trồng ở đảo Java ( khi đó là thuộc địa của họ).
							</p>
							<p>Năm 1723, một sĩ quan hải quân Pháp tên De Clieu được về nghỉ phép ở Paris, đã quyết định đem cây này về xứ Martinique nơi anh trú đóng. Sau nhiều hoạn nạn De Clieu cũng trồng được cây cà phê ở một nơi kín đáo với 3 thủy thủ canh gác ngày đêm. Hơn 50 năm sau, Pháp trở thành đối thủ cạnh tranh gay gắt với Hà Lan, bất đồng xảy ra không thể giải quyết họ nhờ đến chính quyền Brasil đứng ra dàn xếp. Đây là cơ hội, với những quỷ kế, Brasil đã mang được hạt giống về nước và đây là khởi đầu cho giống cà phê trồng tại Brasil, biến các quốc gia Trung, Nam Mỹ trở thành những đế quốc cà phê lớn bậc nhất thế giới.
							</p>
							<p>Trong khi đó, vào năm 1660 cà phê được người Hà Lan truyền vào Bắc Mỹ ở vùng Amsterdam. Bốn năm sau, người Anh chiếm vùng này và đặt tên là New York, cà phê trở thành một thức uống quen thuộc chỉ dành cho giới thượng lưu trong khi trà là thức uống phổ thông trong mọi tầng lớp. Thế nhưng đến năm 1773, khi Anh đánh thuế trà và người dân Mỹ nổi lên chống lại thì tình hình thay đổi. Người Mỹ giả dạng dân da đỏ tấn công những tàu chở trà đem đổ xuống b iển. Biến cố lịch sử dưới tên Boston Tea Party đã làm cho người Mỹ nghiêng qua uống cà phê và chẳng bao lâu thức uống này trở thành quốc ẩm.</p>
						</div>
					</article>
				</div>
			</div>
		</section>
		<section class="block">
			<div background class="block-side block-side--image">
				<img src="<?php echo $home_url; ?>/images/thumbnails/800/532/promo/1/8.jpg" alt="image">
			</div>
			<div class="block-side">
				<div class="block-side-inner">
					<article class="article">
						<header>
							<h1 data-message="text.home.introduce.coffeTree.history.title"></h1>
							<hr class="separator">
						</header>
							<div class="content" style=" overflow-y: scroll; height: 380px" data-message="text.home.introduce.coffeTree.history.content">
							<p>Lần đầu tiên cà phê được đưa vào Việt Nam vào năm 1875, giống Arabica được người Pháp mang từ đảo Bourton sang trồng ở phía Bắc sau đó lan ra các tỉnh miền Trung như Quảng Trị, Bố Trạch, … Sau thu hoạch chế biến dưới thương hiệu “Arabica du Tonkin”, cà phê được nhập khẩu về Pháp. Sau khi chiếm nước ta thực dân Pháp thành lập các đồn điền cà phê như Chinê, Xuân Mai, Sơn Tây chúng canh tác theo phương thức du canh du cư nên năng suất thấp giảm từ 400 – 500 kg/ha những năm đầu xuống còn 100 – 150 kg/ha khi càng về sau. Để cải thiện tình hình, Pháp du nhập vào nước ta hai giống mới là cà phê vối (C. robusta) và cà phê mít ( C. mitcharichia) vào năm 1908 để thay thế, các đồn điền mới lại mọc lên ở phía Bắc như ở Hà Tĩnh (1910), Yên Mỹ (1911, Thanh Hoá), Nghĩa Đàn (1915, Nghệ An). Thời điểm lớn nhất (1946 – 1966) đạt 13.000 ha. Năm 1925, lần đầu tiên được trồng ở Tây Nguyên, sau giải phóng diện tích cà phê cả nước khoảng 20.000 ha, nhờ sự hỗ trợ vốn từ quốc tế, cây cà phê dần được chú trọng, đến năm 1980 diện tích đạt 23.000 ha, xuất khẩu trên 6000 tấn. Bản kế hoạch ban đầu được xây dựng năm 1980 đặt mục tiêu cho ngành cà phê Việt Nam có khoảng 180 nghìn ha với sản lượng 200 nghìn tấn. Sau đó, bản kế hoạch này đã nhiều lần sửa đổi. Các con số cao nhất dừng lại ở mức 350 nghìn ha với sản lượng 450 nghìn tấn (VICOFA, 2002).
							</p>
							<p>Trận sương muối năm 1994 ở Brasil đã phá huỷ phần lớn diện tích cà phê ở nước này, cộng hưởng đợt hạn hán kéo dài năm 1997 đã làm nguồn cung trên toàn thế giới sụp giảm mạnh, giá tăng đột biến đã khích lệ mở rộng diện tích cà phê ở Việt Nam, đầu tư kỹ thuật canh tác thâm canh, chuyên canh, … nhờ đó diện tích và sản lượng tăng nhanh, trung bình 23,9%/năm, đưa tổng diện tích cây cà phê năm 2000 lên đến 516,7 nghìn ha, chiếm 4,14% tổng diện tích cây trồng của Việt Nam, đứng thứ ba chỉ sau hai loại cây lương thực chủ lực là lúa (chiếm 61,4%) và ngô (chiếm 5,7%). Trong thập kỷ 90 thế kỷ XX, sản lượng tăng lên trên 20%/năm (và các năm 1994, 1995, 1996 sản lượng tăng thậm chí còn cao hơn với tỷ lệ lần lượt là 48,5%, 45,8% và 33%). Năm 2000, Việt Nam có khoảng 520 nghìn ha cà phê, tổng sản lượng đạt 800 nghìn tấn. Nếu so với năm 1980, diện tích cà phê của Việt Nam năm 2000 đã tăng gấp 23 lần và sản lượng tăng gấp 83 lần. Mức sản lượng và diện tích vượt xa mọi kế hoạch trước đó và suy đoán của các chuyên gia trong nước và quốc tế.
							</p>
							<p>Cho đến nay sản lượng cà phê cả nước chiếm 8% sản lượng nông nghiệp, chiếm 25% giá trị xuất khẩu và là nước xuất khẩu cà phê Robusta lớn nhất thế giới với hai tỉnh có diện tích canh tác lớn nhất là ĐăkLăc và Gia Lai, mang lại việc làm ổn định, thu nhập cao cho hàng triệu người. Góp phần ổn định kinh tế xã hội ở những vùng xa xôi hẻo lánh, dân tộc ít người, …</p>
						</div>
					</article>
				</div>
			</div>
		</section>
	</div>
	<?php 
        include '../../base/footer.html';
    ?>
</body>

</html>
