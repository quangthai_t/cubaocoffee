<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9" data-placeholder-focus="false"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" prefix="og: http://ogp.me/ns#" class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link type="text/plain" rel="author" href="/humans.txt">
    <title>Contact us - VietSam Coffee</title>
   <!-- <base href="" /> -->
    <?php include '../base/header.html';?>
    <script type="text/javascript" data-no-defer>
    window.jsErrors = [];
    window.onerror = function(errorMessage) {
        document.write('<div data-ca-debug="1" style="border: 2px solid red; margin: 2px;">' + errorMessage + '</div>');
    }
    </script>
</head>
<?php
	use PHPMailer\PHPMailer\PHPMailer;
	if(isset($_POST['submit'])) {
		$to      = 'svcntt.uit@gmail.com';
		$subject = isset($_POST['subject']) ? $_POST['subject'] : "Website handle request from customer";
		$message = isset($_POST['message']) ? $_POST['message'] : "Mail content is empty";
		$from 	 = isset($_POST['from']) ? $_POST['from'] : "svcntt.uit@gmail.com";
		$from_firstname = isset($_POST['from']) ? $_POST['from'] : "";
		$from_lastname = isset($_POST['from']) ? $_POST['from'] : "anonymous";
		$from_name = $from_firstname.$from_lastname;
		$headers = 'From: '.$from. "\r\n" .
		    'Reply-To: '.$to. "\r\n" .
		    'X-Mailer: PHP/' . phpversion();
		try {
			$mail = new PHPMailer(true);
		} catch (Exception $e) {
		    echo 'Caught exception: ',  $e->getMessage(), "\n";
		}

		$mail->IsSMTP();
		$mail->Host = 'smtp.gmail.com';
    	$mail->Port = 465; 
    	$mail->Username = 'svcntt.uit@gmail.com';  
    	$mail->Password = "chandoibome1994";           

		$mail->From = $from;
		$mail->FromName = $from_name;

		$mail->addAddress($to, "VietSam Coffee");

		//Provide file path and name of the attachments

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;

		if(!$mail->send()) 
		{
		    echo "Mailer Error: " . $mail->ErrorInfo;
		} 
		else 
		{
		    echo "Message has been sent successfully";
		}

	}
?> 
<body class="is-loading l-caterings">
    <!--[if lt IE 8]><p class="m-dopamine m-dopamine-oldies">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
    <!-- TOPBAR-->
    <div id="ajax_overlay" class="ty-ajax-overlay"></div>
    <div id="ajax_loading_box" class="ty-ajax-loading-box"></div>
    <div class="cm-notification-container notification-container">
    </div>
    <?php
        include '../base/menu.html';
     ?>
  <header background="<?php echo $home_url;?>/images/thumbnails/background/lienhe.png" class="header header--tall header--faded is-loading">
		<div class="alignbox">
			<div class="alignbox-item alignbox-item--middle u-ta-center">
				<div class="header-suptitle" data-message="text.contact.title.header1">Liên lạc với</div>
				<div class="header-subtitle" data-message="text.contact.title.header2"> Gửi email cho chúng tôi hoặc tìm chúng tôi trên phương tiện truyền thông xã hội </div>
			</div>
		</div>
	</header>
	<div class="no-container block clearfix">
		<div class="block-side block-side--pull block-side--content">
			<div class="block-side-inner">
				<article class="article">
					<header>
						<h1><span>VietSam Coffee</span>Contact Form</h1>
						<hr class="separator">
					</header>
					<form action="<?php echo $home_url;?>/contact/index.php" method="POST" name="forms_form" enctype="multipart/form-data"><input type="hidden" name="fake" value="1" /><input type="hidden" name="page_id" value="15" />
						<div class="row">
							<div class="col-md-6">
								<div class="form-group"><label for="elm_4" class="cm-required control-label">First Name <sup>*</sup></sup></label><input id="elm_4" class="form-control cm-required" type="text" name="mail_firstname" value="" /></div>
							</div>
							<div class="col-md-6">
								<div class="form-group"><label for="elm_5" class="cm-required control-label">Last Name <sup>*</sup></sup></label><input id="elm_5" class="form-control cm-required" type="text" name="mail_lastname" value="" /></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group"><label for="elm_6" class="cm-required cm-email control-label">Email <sup>*</sup></sup></label><input id="elm_6" class="form-control cm-required" type="email" name="mail_form" value="" /></div>
							</div>
							<div class="col-md-6">
								<div class="form-group"><label for="elm_7" class=" control-label">Phone </sup></label><input id="elm_7" class="form-control " type="text" name="form_values[7]" value="" /></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group form-group--quadruple-height"><label for="elm_8" class="cm-required control-label">Message <sup>*</sup></sup></label><textarea id="elm_8" class="form-control cm-required" name="mail_message"></textarea></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group u-ta-right"><input type="submit" value="Send" class="button button--wider button--orange button--hover-dark" name="submit"></div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<h2>Or</h2>
								<hr class="separator">
								<br>
								<div class="form-group u-ta-left">
									<i class="fa fa-home"></i> <span>125 Revolyutsionnaya ulitsa, flat 25, 443079 Samara city, Samara Oblast, Russia.</span>
								</div>
								<div class="form-group u-ta-left">
									<i class="fa fa-phone"></i><span>+7 937188 68 68</span>
								</div>
								<div class="form-group u-ta-left">
									<i class="fa fa-envelope-o"></i><span>vietsamsamara@mail.ru</span>
								</div>
							</div>
						</div>
					</form>
				</article>
			</div>
		</div>
		<div class="block-side block-side--contact block-side--push block-side--content">
			<div class="block-side-inner">
				<article class="article">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2388.5265268696307!2d50.24833711583395!3d53.22633727995411!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x41661bdfec98e5df%3A0x9096a0440c7121b5!2sKrasnodonskaya+Ulitsa%2C+61%2C+Samara%2C+Samarskaya+oblast&#39;%2C+Nga%2C+443009!5e0!3m2!1svi!2s!4v1517730120914" width="100%" height="800px" frameborder="0" style="border:0" allowfullscreen=""></iframe>
				</article>
			</div>
		</div>
	</div>
	<?php 
        include '../base/footer.html';
    ?>

</body>

</html>					