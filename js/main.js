function Language() {
	var locale = "";
	var language = null;
	this.setLanguage = function (lang) {
		$.ajaxSetup({
			headers: {
				'Accept-Language': lang
			}
		})
        localStorage.setItem('langConfig', JSON.stringify({
            lang: lang
        }));
	}
	this.getLocale = function () {
		return this.locale;
	}
	this.init = function (locale) {
		if (!locale) {
			this.locale = 'vi';
			this.selectLanguage(this.locale)
            $.ajaxSetup({
                headers: {
                    'Accept-Language': locale
                }
            })
		}
		else {
			this.locale = locale;
			switch (locale) {
				case 'vi': this.language = lang_vi; break;
				case 'en': this.language = lang_en; break;
				case 'ru': this.language = lang_ru; break;
				default: this.language = lang_vi;
			}
			$.ajaxSetup({
				headers: {
					'Accept-Language': locale
				}
			})
			localStorage.setItem('langConfig', JSON.stringify({
				lang: locale
			}));
			this.language = this.selectLanguage(this.locale);
		}

		var language = this.selectLanguage();
		var elsText = document.querySelectorAll("*[data-message]");
		var elsPlaceHolder = document.querySelectorAll("*[data-placeholder]");
		function getString(language, message) {
			return language[message];
		}
		elsText.forEach(function (el, index) {
			var message = $(el).data('message');
			$(el).html(getString(language, message));
		})
		elsPlaceHolder.forEach(function (el, index) {
			var message = $$(el).data('placeholder');
			$(el).attr("placeholder", getString(language, message));
		})
	}
	this.getMessage = function (title) {
		this.language = this.selectLanguage();
		return this.getString(this.language, title);
	}
	this.getString = function getString(language, message) {
			return language[message];
	}
	this.selectLanguage = function () {
		if (this.locale === 'vi'){
			return lang_vi;
		}else if(this.locale === 'en'){
			return lang_en;
		}else{
			return lang_ru;
		}
	}
}

var language;

$(function (e) {
	window.location.href.split('#')[0];
	language = new Language();
    var langConfig = localStorage.getItem('langConfig');
    try {
        langConfig = JSON.parse(langConfig);
        if (langConfig == null) {
        	langConfig = {
        		lang : "ru"
        	}
        }

        $("#switchLanguage").val(langConfig.lang);

        language.init(langConfig.lang);
	
        $.ajaxSetup({
            headers: {
                'Accept-Language': langConfig.lang
            }
        });
    } catch (e) {
        console.log(e);
    }
});

function switchLanguage(e) {
	var currentLang = $("#switchLanguage").find('option:selected').attr('value');
	language.init(currentLang);
	location.reload();
}	

function switchLanguageHeader(chooseLang) {
	language.init(chooseLang);
	location.reload();
}