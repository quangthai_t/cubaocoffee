<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9" data-placeholder-focus="false"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" prefix="og: http://ogp.me/ns#" class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Policy of quantity - VietSam Cafe</title>
    <link type="text/plain" rel="author" href="/humans.txt">
   <!-- <base href="" /> -->
    <?php include '../base/header.html';?>
    <style>
        .gallery-item{
            padding-bottom:39.3%;
        }
    </style>
    <script type="text/javascript" data-no-defer>
    window.jsErrors = [];
    window.onerror = function(errorMessage) {
        document.write('<div data-ca-debug="1" style="border: 2px solid red; margin: 2px;">' + errorMessage + '</div>');
    }
    </script>
</head>

<body class="is-loading l-caterings">
    <!--[if lt IE 8]><p class="m-dopamine m-dopamine-oldies">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
    <!-- TOPBAR-->
    <div id="ajax_overlay" class="ty-ajax-overlay"></div>
    <div id="ajax_loading_box" class="ty-ajax-loading-box"></div>
    <div class="cm-notification-container notification-container">
    </div>
    <?php include '../base/menu.html';?>
    <header background="<?php echo $home_url;?>/images/thumbnails/background/chinhsachchatluong.png" class="header header--tall header--faded is-loading">
        <div class="alignbox">
            <div class="alignbox-item alignbox-item--middle u-ta-center">
                <div class="header-suptitle" data-message="text.introduce.policy.header1">Chính Sách và Chất Lượng</div>
                <div class="header-title" data-message="text.introduce.policy.header2">Cam kết về chất lượng sản phẩm và cung cấp các chứng chỉ của công ty</div>
            </div>
        </div>
    </header>
   <div class="no-container">
        <section class="block">
            <img src="<?php echo $home_url;?>/images/policies/20160818_154603.png" alt="" class="block-side undefined block-side--pull block-side--image">
            <div class="block-side block-side--push" style="height: 497px;">
                <div class="block-side-inner">
                    <article class="article">
                        <header>
                            <h1 data-message="text.policies.promise.title"><span>VietSam</span>Cam kết</h1>
                            <hr class="separator">
                        </header>
                        <div class="content" data-message="text.introduce.policy.commitment.content">
                            <p data-message="text.policies.promise.sub-title">Công ty cam kết chất lượng sản phẩm:</p>
                            <ul data-message="text.policies.promise.content">
                                <li>Cà phê được trồng, canh tác, thu hoạch và giám sát bởi tổ chức quốc tế UTZ.</li>
                                <li>Quá trình chế biến, chiết xuất hoàn toàn áp dụng quy trình tiêu chuẩn và được đăng ký tại trung tâm kiểm định chất lượng.</li>
                                <li>Vật liệu đầu vào có nguồn gốc rõ ràng và được sản xuất bởi các nhà sản xuất hàng đầu ở Việt Nam và các nước tiên tiến khác.</li>
                                <li>Quá trình chế biến và đóng gói để tuân thủ các điều kiện sản xuất ISO 14000.</li>
                            </ul>
                        </div>
                    </article>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <section class="titlebar titlebar--cup">
                    <i class="titlebar-icon"></i>
                    <h3 class="titlebar-title" data-message="text.policies.certification.title">Giấy chứng nhận chất lượng</h3>
                    <div align="center">
                        <hr class="separator_long">
                    </div>
                </section>
                <div class="col-md-12 col-sm-12" style="margin: 1em 0;">
                    <div data-mh="roast-gallery">
                        <div class="gallery clearfix">
                            <a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/polices/police_1.jpg" background _sizeup class="gallery-item">
                                    <img src="<?php echo $home_url;?>/images/policies/police_2.jpg" alt="gallery-item">
                                </a>
                            <a data-imagelightbox="caterings-gallery" href="<?php echo $home_url;?>/images/policies/police_3.jpg" background _sizeup class="gallery-item">
                                    <img src="<?php echo $home_url;?>/images/policies/police_3.jpg" alt="gallery-item">
                                </a>
                            <a data-imagelightbox="caterings-gallery" href="/images/sanpham/certificate_2.png" background _sizeup class="gallery-item">
                                    <img src="<?php echo $home_url;?>/images/sanpham/certificate_2.png" alt="gallery-item">
                                </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<?php 
        include '../base/footer.html';
    ?>
</body>

</html>