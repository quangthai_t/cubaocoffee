<!DOCTYPE html>
<!--[if lt IE 7]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html lang="en" prefix="og: http://ogp.me/ns#" class="no-js lt-ie9" data-placeholder-focus="false"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" prefix="og: http://ogp.me/ns#" class="no-js">
<!--<![endif]-->

<head>
    <?php include '../base/header.html';?>
    <script type="text/javascript" data-no-defer>
    window.jsErrors = [];
    window.onerror = function(errorMessage) {
        document.write('<div data-ca-debug="1" style="border: 2px solid red; margin: 2px;">' + errorMessage + '</div>');
    }
    </script>
</head>

<body class="is-loading l-caterings">
    <!--[if lt IE 8]><p class="m-dopamine m-dopamine-oldies">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->
    <!-- TOPBAR-->
    <div id="ajax_overlay" class="ty-ajax-overlay"></div>
    <div id="ajax_loading_box" class="ty-ajax-loading-box"></div>
    <div class="cm-notification-container notification-container"></div>
    <?php 
        include '../base/menu.html';
    ?>
    <header background="<?php echo $home_url;?>/images/thumbnails/background/tuyendung.png" class="header header--tall header--faded is-loading">
        <div class="alignbox">
            <div class="alignbox-item alignbox-item--middle u-ta-center">
                <!-- <div class="header-suptitle">Vietruss at your next event</div>
                <div class="header-title">Local and international coffee caterings</div>
                <div class="header-subtitle">
                    Providing excellence in coffee hospitality from tradeshow to personal event </div> -->
            </div>
        </div>
    </header>
    <!-- <div class="no-container">
        <section class="block">
>>>>>>> a906f06411aca7d04590ea567a81011f4b1f3f6c
            <div background class="block-side undefined block-side--image">
                <img src="<?php echo $home_url;?>/images/thumbnails/800/532/promo/1/14.04.11_FD_33.0002-2.jpg" alt="image">
            </div>
            <div class="block-side undefined">
                <div class="block-side-inner">
                    <article class="article">
                        <header>
                            <h1>
                                                                        <span>From inception to execution</span>                                    Bespoke event caterings                                                                 </h1>
                            <hr class="separator">
                        </header>
                        <div class="content">
                            <p>Whether planning a conference, corporate off-site, wedding, or trade show, let Boreal Coffee cater for all of your specialty coffee needs. Working with leading coffee machine manufacturer La Marzocco since 2008 and our own direct-traded and in-house roasted beans, Boreal will provide you with top quality espresso-based beverages throughout your event.
                            </p>
                            <p>We are also happy to arrange a brew bar and an opportunity for guests to cup, for a special experience. Boreal manages logistics, transport, installation and removal of the machines and is staffed only by our certified baristas to ensure quality and professionalism are the highest possible.
                            </p>
                        </div>
                        <footer>
                            <div class="row">
                                <div class="col-sm-12">
                                </div>
                            </div>
                        </footer>
                    </article>
                </div>
            </div>
        </section>
        <section class="block">
            <div background class="block-side block-side--pull block-side--image">
                <img src="<?php echo $home_url;?>/images/thumbnails/800/532/promo/1/IMG_7626-2.jpg" alt="image">
            </div>
            <div class="block-side block-side--push">
                <div class="block-side-inner">
                    <article class="article">
                        <header>
                            <h1>
                                                                        <span>Working only with the best since 2008</span>                                  La Marzocco                                                                 </h1>
                            <hr class="separator">
                        </header>
                        <div class="content">
                            <p>La Marzocco est notre partenaire pour les machines à café. Ces machines fabriquées à la main à Florence font partie des meilleures sur le maché «specialty coffee».</p>
                            <p>De 2000 à 2008, La Marzocco était le sponsor officiel pour les championnats de monde Barista (WBC), la compétition internationale la plus prestigieuse qui met en avant la culture café à travers le monde. Nous installons nos machines à café dans votre environnement. Toutes les spécifications techniques seront fournies afin que vous puissiez préparer le stand avant notre arrivée.</p>
                        </div>
                    </article>
                </div>
            </div>
        </section>
        <section class="block">
            <div background class="block-side undefined block-side--image">
                <img src="<?php echo $home_url;?>/images/thumbnails/800/532/promo/1/13_0911-0229_NatashaCarrion.jpg" alt="image">
            </div>
            <div class="block-side undefined">
                <div class="block-side-inner">
                    <article class="article">
                        <header>
                            <h1>
                                                                        <span>Explore our complete offering</span>                                  Vietruss Caterings                                                                    </h1>
                            <hr class="separator">
                        </header>
                        <nav class="block-tab visible-xs visible-sm">
                            <div class="select">
                                <select tab-dropdown>
                                    <option value="#tab31" selected="selected">Coffee beans</option>
                                    <option value="#tab32">Drinks on offer</option>
                                    <option value="#tab33">Expertly trained baristas</option>
                                    <option value="#tab34">Coffee passion</option>
                                </select>
                            </div>
                        </nav>
                        <nav class="block-tab hidden-xs hidden-sm">
                            <a tab-selector href="#tab31" class="is-active">Coffee beans</a>
                            <a tab-selector href="#tab32">Drinks on offer</a>
                            <a tab-selector href="#tab33">Expertly trained baristas</a>
                            <a tab-selector href="#tab34">Coffee passion</a>
                        </nav>
                        <div tab-content="tab31" class="content is-active">
                            <p>We provide all of our catering clients with direct sourced, freshly roasted coffee beans of high quality.</p>
                            <p>The coffee beans are always included in our offers. On request, we can work with pure specific origins.</p>
                        </div>
                        <div tab-content="tab32" class="content">
                            <p>In our experience and according to your needs, we can provide a complete menu of coffee-based beverages.</p>
                            <p>All our coffee beverages comply with Italian industry standards (size, coffee proportion of milk, etc.).</p>
                        </div>
                        <div tab-content="tab33" class="content">
                            <p>All of our baristas are trained and have an excellent understanding of specialty coffee culture and adherence to quality. Most of them are certified by the SCAE (Specialty Coffee Association of Europe).</p>
                            <p>They focus in particular on the preparation of espresso, milk texture and alternative preparation techniques (brewing).</p>
                        </div>
                        <div tab-content="tab34" class="content">
                            <p>Preparing perfect coffee is not just our job, it's also our passion. Discovering the world of coffee is a unique satisfaction and we take it seriously while bringing an element of fun to events.</p>
                        </div>
                    </article>
                </div>
            </div>
<<<<<<< HEAD
        </section> -->
    </div>
    <?php 
        include '../base/footer.html';
    ?>

</body>

</html>